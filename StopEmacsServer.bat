:: ============================================================================
:: Batch File to STOP Emacs Daemon
:: Assumes HOME variable has been set through Windows Environment Variables
:: ============================================================================

:: Stop Emacs Daemon
emacsclient.exe -eval "(kill-emacs)"