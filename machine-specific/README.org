#+TITLE: Machine-Specific Folder README

This folder is for databases and other files which should be generated and maintained on a per-machine basis. Contents in this folder are ignored by GIT and not synced.
