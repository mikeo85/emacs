:: ============================================================================
:: Batch File to Start Emacs Daemon on System Startup
:: Assumes HOME variable has been set through Windows Environment Variables
:: ============================================================================

:: Clean previous server file info first, in case of dirty shutdown
del /Q "%HOME%/.emacs.d/server/*"
:: Start Emacs Daemon
runemacs.exe --daemon