;; ########################################
;; #  _______                                 
;; # (_______)                                
;; #  _____    ____   _____   ____   ___      
;; # |  ___)  |    \ (____ | / ___) /___)     
;; # | |_____ | | | |/ ___ |( (___ |___ |     
;; # |_______)|_|_|_|\_____| \____)(___/      
;; #
;; #  _______                   ___  _        
;; # (_______)                 / __)(_)       
;; #  _         ___   ____   _| |__  _   ____ 
;; # | |       / _ \ |  _ \ (_   __)| | / _  |
;; # | |_____ | |_| || | | |  | |   | |( (_| |
;; #  \______) \___/ |_| |_|  |_|   |_| \___ |
;; #                                   (_____|
;; #
;; #  MIKE'S EMACS CONFIG FILE
;; #  ------------------------
;; #
;; #    Author:  Mike Owens
;; #    Website: https://michaelowens.me
;; #    GitLab:  mikeo85  GitHub: mikeo85
;; #    Twitter: @quietmike8192
;; #
;; ########################################

;; ASCII Art Source:
;; https://www.patorjk.com/software/taag/#p=display&h=0&v=0&f=Rounded&t=Emacs%20Config

;; ########################################
;; # PRE-STARTUP CONFIG
;; ########################################

;; Provide backtrace for any errors
(setq debug-on-error t)

;; ## CORE CAPABILITIES PICKER
;; ========================================
;; Out-of-the-box settings are below. By default,
;; core capabilities with dependencies are disabled
;; for the simplest first-start experience.
;; Use the file 'm-machine-specific-capabilities-picker.el'
;; (generated on first run and not tracked by git)
;; to change these settings on a machine-specific basis.
;; For each variable below, options are 't' or 'nil'.

;; ### DEFAULT SETTINGS
;; ----------------------------------------
(setq m-use-private-config nil)
(setq m-use-server-mode nil)
(setq m-use-advanced-ui-config nil)
(setq m-use-org-core t)
(setq m-use-org-pkms nil) ;; depends on org-core
(setq m-is-for-work nil)

;; ### CUSTOMIZE PER-MACHINE CAPABILITIES
;; ----------------------------------------
;; If the machine-specific settings file exists, load it to
;; overwrite default settings with machine-specific config.
;; Otherwise, create the empty template file.
(setq m-capabilities-picker-file "m-machine-specific-capabilities-picker.el")
(if (equal (string-to-number (shell-command-to-string (concat "ls ~/.emacs.d | grep " m-capabilities-picker-file " | wc -l"))) 0)
		(progn
			(setq m-picker-default-text
						";; ############################################################
;; #
;; #  MACHINE-SPECIFIC CAPABILITIES PICKER
;; #  ------------------------
;; #
;; #  Use this file to override the default settings
;; #  in the 'CORE CAPABILITIES PICKER' section of
;; #  init.el.
;; #
;; #  Capabilities are enabled with 't' option
;; #  and disabled with 'nil' option.
;; #
;; #  Examples:
;; #    (setq variable-one t)
;; #    (setq variable-two nil)
;; #
;; #  *This file is not tracked in git.*
;; #
;; ############################################################")
			(shell-command (concat "cat > ~/.emacs.d/" m-capabilities-picker-file " << EOF
" m-picker-default-text "
EOF"))
			)
	(load (concat "~/.emacs.d/" m-capabilities-picker-file))
	)


;; ########################################
;; # STARTUP
;; ########################################

(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
            '("melpa" . "http://melpa.org/packages/"))
(package-initialize)
(package-refresh-contents)

;; ## SETUP USE-PACKAGE
;; ========================================
;; REF: https://github.com/jwiegley/use-package
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

;; Should set before loading 'use-package'
(eval-and-compile
  (setq use-package-always-ensure nil)
  (setq use-package-always-defer nil)
  (setq use-package-always-demand nil)
  (setq use-package-expand-minimally nil)
  (setq use-package-enable-imenu-support t))

(eval-when-compile
  (require 'use-package))
;; (setq use-package-always-ensure t)

(use-package diminish
  :ensure t)

;; ## LOAD CUSTOM MODULES PATH
;; ========================================
(add-to-list 'load-path "~/.emacs.d/modules/")

;; ## LOAD PRIVATE CONFIG PATH
;; ========================================
(when (equal m-use-private-config t)
	(add-to-list 'load-path "~/.emacs.d/privateemacs/"))

;; ## LOAD CUSTOM.EL
;; ========================================
;; REF: PS
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

;; ########################################
;; # BASIC SETUP AND UI
;; ########################################

;; ## PERSONAL INFO
;; ========================================
(setq user-full-name "Mike Owens"
      user-mail-address "mikeowens@fastmail.com")

;; ## THEME
;; ========================================

;; Stop asking if it is safe to load a theme
(setq custom-safe-themes t)

;; ### EMACS-TRON-THEME
;; ----------------------------------------
;;     My custom theme
(add-to-list 'custom-theme-load-path "~/.emacs.d/otherpackages/emacs-tron-theme")
(load-theme 'tron t)
(if (equal system-type 'darwin)
		(progn (set-face-attribute 'default nil :height 180))
	(progn (set-face-attribute 'default nil :height 140))
	)

;; ## FONTS
;; ========================================

;; ### HACK FONT
;; ----------------------------------------
;; https://sourcefoundry.org/hack/
;; abcdefghijklmnopqrstuvwxyz 12345
;; ABCDEFGHIJKLMNOPQRSTUVWXYZ 67890
;; {}[]()<>$*-+=/#_%^@\&|~?'"`!,.;:
(when (member "Hack" (font-family-list))
  (add-to-list 'initial-frame-alist '(font . "Hack-18"))
  (add-to-list 'default-frame-alist '(font . "Hack-18"))
	)

;; ## UI TWEAKS
;; ========================================
(setq inhibit-startup-message t) ;; hide the startup message
(setq initial-scratch-message nil) ;; remove message from scratch buffer
(tool-bar-mode -1)
(scroll-bar-mode -1)
(blink-cursor-mode 0)
(setq ring-bell-function 'ignore)
(fset 'yes-or-no-p 'y-or-n-p) ;; Don't make me type 'yes'
(setq delete-by-moving-to-trash t) ;; source: https://masteringemacs.org/article/making-deleted-files-trash-can

(setq use-file-dialog nil)
(setq use-dialog-box t)  ;; only for mouse events

;; #### Prompt before killing Emacs
(setq confirm-kill-emacs 'y-or-n-p)

;; #### Require UTF-8 Encoding
(setq locale-coding-system 'utf-8) ; pretty
(set-terminal-coding-system 'utf-8) ; pretty
(set-keyboard-coding-system 'utf-8) ; pretty
(set-selection-coding-system 'utf-8) ; please
(setq-default buffer-file-coding-system 'utf-8-unix)
(setq-default default-buffer-file-coding-system 'utf-8-unix)
(set-default-coding-systems 'utf-8-unix)
(prefer-coding-system 'utf-8-unix)
(when (eq system-type 'windows-nt)
  (set-clipboard-coding-system 'utf-16le-dos))

(setq show-paren-delay 0) ;; Show Paren Mode - https://www.emacswiki.org/emacs/ShowParenMode
(show-paren-mode 1)

;; #### Backup Files Management
;;      Stop Emacs from "littering" your folder with #file# and file~
;;      These configurations will stop Emacs creating backup files, etc.
;; ##### Avoid #file.org# to appear
(auto-save-visited-mode)
(setq create-lockfiles nil)
;; ##### Avoid filename.ext~ to appear
(setq make-backup-files nil)

;; #### Automatically Revert Buffers
;; auto revert mode
(global-auto-revert-mode 1)
;; auto refresh dired when file changes
(add-hook 'dired-mode-hook 'auto-revert-mode)

;; #### Remove-DOS-EOL:
;;      Do not show ^M in files containing mixed UNIX and DOS line endings.
;;      Ref: https://stackoverflow.com/questions/730751/hiding-m-in-emacs
(defun mlo/remove-dos-eol ()
  "Do not show ^M in files containing mixed UNIX and DOS line endings."
  (interactive)
  (setq buffer-display-table (make-display-table))
  (aset buffer-display-table ?\^M []))
(add-hook 'text-mode-hook 'mlo/remove-dos-eol)

;; ### LINE NUMBERS
;; ----------------------------------------
(global-linum-mode t);; enable line numbers globally
(eval-after-load "linum"
  '(set-face-attribute 'linum nil :height 120)) ;; https://unix.stackexchange.com/questions/29786/font-size-issues-with-emacs-in-linum-mode/146781#146781

;; Disable line numbers for some modes
(dolist (mode '(term-mode-hook
								shell-mode-hook
								eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; ### GENERAL KEYBINDINGS
;; ----------------------------------------
(define-key global-map (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "C-z") 'undo) ; Ctrl+z
(global-set-key (kbd "C-o") 'other-window) ;; Mastering Emacs book p. 92
;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-unset-key (kbd "C-x C-l")) ; Unset this because I never use it and often accidentally bump it


;; #### Which Key
;;      Guide to available keybinding completions
(use-package which-key
  :ensure t
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 1))

;; ## MACOS BASIC SETUP
;; ========================================
;; https://medium.com/really-learn-programming/configuring-emacs-on-macos-a6c5a0a8b9fa
(when (equal system-type 'darwin)

  ;; SYSTEM CONFIG STUFF
  (setq mac-command-modifier 'meta)
  (setq mac-option-modifier 'meta)
  (setq mac-right-option-modifier 'control)
  (global-set-key (kbd "<home>") 'move-beginning-of-line)
  (global-set-key (kbd "<end>") 'end-of-visual-line)
  ;; (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
  (add-to-list 'default-frame-alist '(ns-appearance . dark))
  (set-fontset-font t 'symbol (font-spec :family "Apple Symbols") nil 'prepend)
  (set-fontset-font t 'symbol (font-spec :family "Apple Color Emoji") nil 'prepend)
  (setq shell-file-name "/bin/zsh")
	;; Sync Emacs PATH from SHELL. Ref: https://github.com/purcell/exec-path-from-shell
	(use-package exec-path-from-shell
		:ensure t
		:init
		(exec-path-from-shell-initialize))
  (setq default-directory "~/")
	(when (featurep 'ns)
		(defun mlo/raise-emacs ()
			"Raise Emacs."
			(ns-do-applescript "tell application \"Emacs\" to activate"))(defun mlo/raise-emacs-with-frame (frame)
			"Raise Emacs and select the provided frame."
			(with-selected-frame frame
				(when (display-graphic-p)
					(mlo/raise-emacs))))(add-hook 'after-make-frame-functions 'mlo/raise-emacs-with-frame)(when (display-graphic-p)
			(mlo/raise-emacs)))
  (setq org-image-actual-width 600)

	;; ## Manage mouse scroll speed
	;; ----------------------------------------
	(setq mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control) . nil)))
	(setq mouse-wheel-progressive-speed nil)
	)

;; ########################################
;; # SERVER MODE
;; ########################################

;; Various emacs server config, primarily from
;; https://www.emacswiki.org/emacs/EmacsAsDaemon

(when (equal m-use-server-mode t)
	
	;; ## Start Emacs Server
	;; ========================================
	(require 'server)
	(unless (server-running-p) (server-start))

	;; ## Linux-Specific Server Config
	;; ========================================
	(when (eq system-type 'gnu/linux)
		 ;; ### Client-Save-Kill-Emacs
		 ;; ----------------------------------------
		 (defun mlo/client-save-kill-emacs(&optional display)
			 ;; This is a function that can be used to shutdown save buffers and 
			 ;; shutdown the emacs daemon. It should be called using 
			 ;; emacsclient -e '(mlo/client-save-kill-emacs)'.  This function will
			 ;; check to see if there are any modified buffers or active clients
			 ;; or frame.  If so an x window will be opened and the user will
			 ;; be prompted.
			 (let (new-frame modified-buffers active-clients-or-frames)
				 ;; Check if there are modified buffers or active clients or frames.
				 (setq modified-buffers (mlo/modified-buffers-exist))
				 (setq active-clients-or-frames ( or (> (length server-clients) 1)
																						 (> (length (frame-list)) 1)
																						 ))
				 ;; Create a new frame if prompts are needed.
				 (when (or modified-buffers active-clients-or-frames)
					 (when (not (eq window-system 'x))
						 (message "Initializing x windows system.")
						 (x-initialize-window-system))
					 (when (not display) (setq display (getenv "DISPLAY")))
					 (message "Opening frame on display: %s" display)
					 (select-frame (make-frame-on-display display '((window-system . x)))))
				 ;; Save the current frame.
				 (setq new-frame (selected-frame))
				 ;; When displaying the number of clients and frames: 
				 ;; subtract 1 from the clients for this client.
				 ;; subtract 2 from the frames this frame (that we just created)
				 ;; and the default frame.
				 (when ( or (not active-clients-or-frames)
										(yes-or-no-p (format "There are currently %d clients and %d frames. Exit anyway?" (- (length server-clients) 1) (- (length (frame-list)) 2)))) 
					 ;; If the user quits during the save dialog then don't exit emacs.
					 ;; Still close the terminal though.
					 (let((inhibit-quit t))
						 ;; Save buffers
						 (with-local-quit
							 (save-some-buffers))
						 (if quit-flag
								 (setq quit-flag nil)
							 ;; Kill all remaining clients
							 (progn
								 (dolist (client server-clients)
									 (server-delete-client client))
								 ;; Exit emacs
								 (kill-emacs)))
						 ))
				 ;; If we made a frame then kill it.
				 (when (or modified-buffers active-clients-or-frames) (delete-frame new-frame))
				 )
			 )

		 (defun mlo/modified-buffers-exist() 
			 "This function will check to see if there are any buffers
    that have been modified.  It will return true if there are
    and nil otherwise. Buffers that have buffer-offer-save set to
    nil are ignored."
			 (let (modified-found)
				 (dolist (buffer (buffer-list))
					 (when (and (buffer-live-p buffer)
											(buffer-modified-p buffer)
											(not (buffer-base-buffer buffer))
											(or
											 (buffer-file-name buffer)
											 (progn
												 (set-buffer buffer)
												 (and buffer-offer-save (> (buffer-size) 0))))
											)
						 (setq modified-found t)
						 )
					 )
				 modified-found
				 )
			 )

		 ;; ### Dbus Management
		 ;; ----------------------------------------
		 (require 'dbus)

		 (defun mlo/my-register-signals (client-path)
			 "Register for the 'QueryEndSession' and 'EndSession' signals from
    Gnome SessionManager.

    When we receive 'QueryEndSession', we just respond with
    'EndSessionResponse(true, \"\")'.  When we receive 'EndSession', we
    append this EndSessionResponse to kill-emacs-hook, and then call
    kill-emacs.  This way, we can shut down the Emacs daemon cleanly
    before we send our 'ok' to the SessionManager."
			 (setq my-gnome-client-path client-path)
			 (let ( (end-session-response (lambda (&optional arg)
																			(dbus-call-method-asynchronously
																			 :session "org.gnome.SessionManager" my-gnome-client-path
																			 "org.gnome.SessionManager.ClientPrivate" "EndSessionResponse" nil
																			 t "") ) ) )
				 (dbus-register-signal
					:session "org.gnome.SessionManager" my-gnome-client-path
					"org.gnome.SessionManager.ClientPrivate" "QueryEndSession"
					end-session-response )
				 (dbus-register-signal
					:session "org.gnome.SessionManager" my-gnome-client-path
					"org.gnome.SessionManager.ClientPrivate" "EndSession"
					`(lambda (arg)
						 (add-hook 'kill-emacs-hook ,end-session-response t)
						 (kill-emacs) ) ) ) )

		 ;; DESKTOP_AUTOSTART_ID is set by the Gnome desktop manager when emacs
		 ;; is autostarted.  We can use it to register as a client with gnome
		 ;; SessionManager.
		 (dbus-call-method-asynchronously
			:session "org.gnome.SessionManager"
			"/org/gnome/SessionManager" 
			"org.gnome.SessionManager" "RegisterClient" 'mlo/my-register-signals
			"Emacs server" (getenv "DESKTOP_AUTOSTART_ID"))
		 )
		) ;; end SERVER MODE

;; ########################################
;; # CORE OPERATING FUNCTIONALITY
;; ########################################

;; ## WINDOW MANAGEMENT
;; ========================================

;; ### Windmove package
;; ----------------------------------------
;; Ref Mastering Emacspp. 92
;; Allows you to cycle windows in cardinal directions
;; Switch windows with shift key by pressing
;; S-<left>, S-<right>, S-<up>, S-<down>
(windmove-default-keybindings)

;; ### Transpose Frame
;; ----------------------------------------
(use-package transpose-frame
  :ensure t
  :config
  (global-set-key (kbd "<f10>") 'transpose-frame))

;; ## COMPLETION SUPPORT
;; ========================================

;; ### ELDOC
;; ----------------------------------------
;; A very simple but effective thing, eldoc-mode is a MinorMode
;; which shows you, in the echo area, the argument list of the
;; function call you are currently writing. Very handy.
;; By NoahFriedman. Part of Emacs.
(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)
(add-hook 'ielm-mode-hook 'turn-on-eldoc-mode)
(diminish 'eldoc-mode)

;; ### IVY-SWIPER-COUNSEL
;; ----------------------------------------
;; refs: https://writequit.org/denver-emacs/presentations/2017-04-11-ivy.html

;; #### Ivy & Swiper
(use-package ivy
  :ensure t
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(use-package ivy-rich
  :ensure t
  :init
  (ivy-rich-mode 1))

;; #### Counsel
(recentf-mode 1)
(global-unset-key (kbd "C-x C-r"))
(use-package counsel
  :ensure t
  :bind (("M-x" . counsel-M-x)
         ("C-x b" . counsel-ibuffer)
         ("C-x C-f" . counsel-find-file)
				 ("C-x C-r" . counsel-buffer-or-recentf)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history)
				 )
  )

;; #TODO Try 'Helpful' package in connection with Counsel

;; ## SHELL (M-x shell)
;; ========================================
;; Ref: PS
(use-package shell
  :ensure t
  :commands shell-command
  :config
  (setq ansi-color-for-comint-mode t)
  (setq shell-command-prompt-show-cwd t) ; Emacs 27.1
	)

;; ## MAGIT (C-c g)
;; ========================================
(use-package magit
  :ensure t
  :bind ("C-c g" . magit-status))

;; ########################################
;; # ADVANCED UI CONFIG
;; ########################################
(when (eq m-use-advanced-ui-config t)

	;; ## PROJECTILE (C-c p ...)
	;; ========================================
	(use-package projectile
		:diminish projectile-mode
		:ensure t
		)

	(projectile-mode +1)
	(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

	;; ## DASHBOARD
	;; ========================================
	(use-package dashboard
		:ensure t
		:config
		(dashboard-setup-startup-hook)
		(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*"))
					;; Set the title
					dashboard-banner-logo-title "What Good Shall I Do This Day?"
					;; Set the banner
					dashboard-startup-banner "~/.emacs.d/modules/mountain.png"
					;; Value can be
					;; 'official which displays the official emacs logo
					;; 'logo which displays an alternative emacs logo
					;; 1, 2 or 3 which displays one of the text banners
					;; "path/to/your/image.png" which displays whatever image you would prefer

					;; Content is not centered by default. To center, set
					;; dashboard-center-content t

					;; To disable shortcut "jump" indicators for each section, set
					;; dashboard-show-shortcuts nil

					dashboard-items '(
														(agenda . 10)
														;; (projects . 5)
														;; (bookmarks . 5)
														(recents  . 10)
														;; (registers . 5)	
														)
					dashboard-set-navigator t
					dashboard-set-init-info t
					dashboard-init-info "C-c h: Jump to Dashboard Buffer."

					;; A randomly selected footnote will be displayed. To disable it:
					;; dashboard-set-footer nil
					;; Or set custom dashboard footnotes:
					dashboard-footer-icon ""
					dashboard-footer-messages (quote (
																						"
     Do not conform yourselves to this age,
     but be transformed by the renewal of your mind,
     that you may discern what is the will of God,
     what is good and pleasing and perfect."
																						"
     For whoever wishes to save his life will lose it,
     but whoever loses his life for my sake will find it. 
     What profit would there be for one
     to gain the whole world and forfeit his life?"
																						"
     None of us lives for oneself, and no one dies for oneself.
     For if we live, we live for the Lord,
     and if we die, we die for the Lord;
     so then, whether we live or die, we are the Lord’s."
																						"
     And let thy feet millenniums hence
     be set in the midst of knowledge.
     (Tennyson)"
																						"
     Sometimes faith is understanding
     that you cannot understand."
																						"
     All great things are simple,
     and many can be expressed in a single word: 
     freedom; justice; honor; duty; mercy; hope.
     (Churchill)"
																						"
     We need to learn to set our course by the stars,
     not by every passing ship.
     (Bradley)"
																						"
     I will never leave you nor forsake you.
     Be strong and courageous, because you will lead…
     Have I not commanded you? Be strong and courageous.
     Do not be terrified; do not be discouraged, 
     for the Lord your God will be with you wherever you go."
																						"
     What saves a man is to take a step. Then another step.
     It is always the same step, but you have to take it.
     (Antoine de Saint-Exupery)"
																						"
     Happiness is different from pleasure.
     Happiness has something to do with 
     struggling and enduring and accomplishing.
     (George Sheehan)"
																						"
     The credit belongs to the man
     who is actually in the arena."
																						))
					)
		)
	;; Remap Open Dashboard
	;; ref: https://github.com/emacs-dashboard/emacs-dashboard/issues/236#issuecomment-711360926
	(defun mlo/new-dashboard ()
		"Jump to the dashboard buffer, if doesn't exists create one."
		(interactive)
		(switch-to-buffer dashboard-buffer-name)
		(dashboard-mode)
		(dashboard-insert-startupify-lists)
		(dashboard-refresh-buffer))
	(global-set-key (kbd "C-c h") 'mlo/new-dashboard)
	)

;; ########################################
;; # BASIC TEXT EDITING
;; ########################################
;; Note: Org Mode is a stand-alone section

;; ## MARKDOWN
;; ========================================
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "pandoc"))

;; ## OLIVETTI WRITING MODE (C-c o)
;; ========================================
;; Look & Feel for long-form writing.
;; Narrows visible text area in buffer
;; with blank space on either side.
(use-package olivetti
  :ensure t)

(global-set-key (kbd "C-c o") 'olivetti-mode)
(setq olivetti-body-width 90)

;; Enable Olivetti for text-related mode such as Org Mode
;; (add-hook 'text-mode-hook 'olivetti-mode)

;; ## `titlecase-string` Convert string to title case
;; ========================================
;; A function to convert a string to title case,
;; with handlers for punctuation and more.
;; Source: The Programmer’s Way to Write in Title Case Using Emacs Lisp
;; https://hungyi.net/posts/programmers-way-to-title-case/

(require 'cl-lib)
(require 'subr-x)

(defun titlecase-string (str)
  "Convert string STR to title case and return the resulting string."
  (let* ((case-fold-search nil)
         (str-length (length str))
         ;; A list of markers that indicate start of a new phrase within the title, e.g. "The Lonely Reindeer: A Christmas Story"
         (new-phrase-chars '(?: ?. ?? ?\; ?\n ?\r)) ; must be followed by one of  word-boundary-chars
         (immediate-new-phrase-chars '(?\n ?\r))    ; immediately triggers new phrase behavior without waiting for word boundary
         ;; A list of characters that indicate "word boundaries"; used to split the title into processable segments
         (word-boundary-chars (append '(?  ?– ?— ?- ?‑ ?/) immediate-new-phrase-chars))
         ;; A list of small words that should not be capitalized (in the right conditions)
         (small-words (split-string "a an and as at but by en for if in of on or the to v v. vs vs. via" " "))
         ;; Fix if str is ALL CAPS
         (str (if (string-match-p "[a-z]" str) str (downcase str)))
         ;; Reduce over a state machine to do title casing
         (final-state (cl-reduce
                       (lambda (state char)
                         (let* ((result               (aref state 0))
                                (last-segment         (aref state 1))
                                (first-word-p         (aref state 2))
                                (was-in-path-p        (aref state 3))
                                (last-char            (car last-segment))
                                (in-path-p            (or (and (eq char ?/)
                                                               (or (not last-segment) (member last-char '(?. ?~))))
                                                          (and was-in-path-p
                                                               (not (or (eq char ? )
                                                                        (member char immediate-new-phrase-chars))))))
                                (end-p                (eq (+ (length result) (length last-segment) 1)
                                                          str-length))                                          ; are we at the end of the input string?
                                (pop-p                (or end-p (and (not in-path-p)
                                                                     (member char word-boundary-chars))))       ; do we need to pop a segment onto the output result?
                                (segment              (cons char last-segment))                                 ; add the current char to the current segment
                                (segment-string       (apply #'string (reverse segment)))                       ; the readable version of the segment
                                (small-word-p         (member (downcase (substring segment-string 0 -1))
                                                              small-words))                                     ; was the last segment a small word?
                                (capitalize-p         (or end-p first-word-p (not small-word-p)))               ; do we need to capitalized this segment or lowercase it?
                                (ignore-segment-p     (or (string-match-p "[a-zA-Z].*[A-Z]" segment-string)     ; ignore explicitly capitalized segments
                                                          (string-match-p "^https?:" segment-string)            ; ignore URLs
                                                          (string-match-p "\\w\\.\\w" segment-string)           ; ignore hostnames and namespaces.like.this
                                                          (string-match-p "^[A-Za-z]:\\\\" segment-string)      ; ignore windows filesystem paths
                                                          was-in-path-p                                         ; ignore unix filesystem paths
                                                          (member ?@ segment)))                                 ; ignore email addresses and user handles with @ symbol
                                (next-result          (if pop-p
                                                          (concat
                                                           result
                                                           (if ignore-segment-p
                                                               segment-string                                   ; pop segment onto the result without processing
                                                             (titlecase--segment segment-string capitalize-p))) ; titlecase the segment before popping onto result
                                                        result))
                                (next-segment         (unless pop-p segment))
                                (will-be-first-word-p (if pop-p
                                                          (or (not last-segment)
                                                              (member last-char new-phrase-chars)
                                                              (member char immediate-new-phrase-chars))
                                                        first-word-p)))
                           (vector next-result next-segment will-be-first-word-p in-path-p)))
                       str
                       :initial-value
                       (vector nil      ; result stack
                               nil      ; current working segment
                               t        ; is it the first word of a phrase?
                               nil))))  ; are we inside of a filesystem path?
    (aref final-state 0)))

(defun titlecase--segment (segment capitalize-p)
  "Convert a title's inner SEGMENT to capitalized or lower case depending on CAPITALIZE-P, then return the result."
  (let* ((case-fold-search nil)
         (ignore-chars '(?' ?\" ?\( ?\[ ?‘ ?“ ?’ ?” ?_))
         (final-state (cl-reduce
                       (lambda (state char)
                         (let ((result (aref state 0))
                               (downcase-p (aref state 1)))
                           (cond
                            (downcase-p                 (vector (cons (downcase char) result) t))  ; already upcased start of segment, so lowercase the rest
                            ((member char ignore-chars) (vector (cons char result) downcase-p))    ; check if start char of segment needs to be ignored
                            (t                          (vector (cons (upcase char) result) t))))) ; haven't upcased yet, and we can, so do it
                       segment
                       :initial-value (vector nil (not capitalize-p)))))
    (thread-last (aref final-state 0)
      (reverse)
      (apply #'string))))

(defun titlecase-region (begin end)
  "Convert text in region from BEGIN to END to title case."
  (interactive "*r")
  (let ((pt (point)))
    (insert (titlecase-string (delete-and-extract-region begin end)))
    (goto-char pt)))

(defun titlecase-dwim ()
  "Convert the region or current line to title case.
If Transient Mark Mode is on and there is an active region, convert
the region to title case.  Otherwise, work on the current line."
  (interactive)
  (if (and transient-mark-mode mark-active)
      (titlecase-region (region-beginning) (region-end))
    (titlecase-region (point-at-bol) (point-at-eol))))


;; ########################################
;; # ORG MODE
;; ########################################

;; ## CORE ORG MODE FUNCTIONALITY
;; ========================================
(when (eq m-use-org-core t)
	(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
	(use-package org
		:ensure org-plus-contrib)

	;; ### KEYBINDINGS
	;; ----------------------------------------
	;; The following lines are always needed.  Choose your own keys.
	(global-set-key "\C-cl" 'org-store-link)
	(global-set-key "\C-ca" 'org-agenda)
	(global-set-key "\C-cc" 'org-capture)
	(global-set-key "\C-cb" 'org-switchb)

	;; ### BASIC DISPLAY AND BEHAVIOR
	;; ----------------------------------------
	(add-hook 'org-mode-hook 'visual-line-mode)
	(add-hook 'org-mode-hook 'org-indent-mode)

	(setq org-agenda-skip-deadline-if-done t)
	(setq org-agenda-skip-scheduled-if-done t)
	(setq org-deadline-warning-days 0)
	(setq org-scheduled-warning-days 0)
	(setq org-fontify-done-headline t)
	(setq org-log-done (quote time))
	(setq org-log-done-with-time t)
	(setq org-priority-start-cycle-with-default nil)
	(setq org-support-shift-select nil)
	(setq org-id-method 'ts) ;; Use timestamps for org IDs: https://org-roam.discourse.group/t/org-roam-major-redesign/1198/28

	;; #### Display properties
	(setq org-cycle-separator-lines 0)
	(setq org-tags-column 8)
	(setq org-latex-prefer-user-labels t)
	(setq org-ellipsis " [+]")
	(custom-set-faces '(org-ellipsis ((t (:foreground "gray40" :underline nil)))))
	(setq org-display-inline-images t)
	(setq org-redisplay-inline-images t)
	(setq org-startup-with-inline-images "inlineimages")

	;; #### Dim blocked tasks (and other settings)
	(setq org-enforce-todo-dependencies t)

	;; ### TASK MANAGEMENT
	;; ----------------------------------------
	(setq org-todo-keywords
				'((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
					(sequence "PROJ(p)" "GOAL(g)" "|" "COMPLETE(c)")
					(sequence "WAITING(w)" "BACKBURNER(b)" "IDEA(i)" "|" "CANCELLED(C)")))

	;; ####Auto-update tags whenever the state is changed
	;; ~Disabled~ because I don't use it
	(setq org-todo-state-tags-triggers
				'(
					("IDEA" ("ideas" . t))
					;; ("CANCELLED" ("CANCELLED" . t))
					;; ("WAITING" ("BACKBURNER") ("WAITING" . t))
					;; ("BACKBURNER" ("WAITING") ("BACKBURNER" . t))
					;; (done ("WAITING") ("BACKBURNER"))
					;; ("TODO" ("WAITING") ("CANCELLED") ("BACKBURNER"))
					;; ("NEXT" ("WAITING") ("CANCELLED") ("BACKBURNER"))
					;; ("PROJ" ("WAITING") ("CANCELLED") ("BACKBURNER"))
					;; ("AOR" ("WAITING") ("CANCELLED") ("BACKBURNER"))
					;; ("DONE" ("WAITING") ("CANCELLED") ("BACKBURNER"))
					))

	;; #### Archive Done Tasks (custom function)
	;; Function to archive all DONE tasks in a subtree
	;; ref :https://stackoverflow.com/questions/6997387/how-to-archive-all-the-done-tasks-using-a-single-command
	(defun org-archive-done-tasks ()
		(interactive)
		(org-map-entries
		 (lambda ()
			 (org-archive-subtree)
			 (setq org-map-continue-from (org-element-property :begin (org-element-at-point))))
		 "/DONE" 'tree))
	
	;; #### toc-org -- ORG TABLE OF CONTENTS GENERATOR
	;; To use this, add a :TOC: tag to a headline.
	;; Every time you save, the first headline with the :TOC: tag will
	;; be updated with the current table of contents.
	;; See additional instructions at
	;;https://github.com/snosov1/toc-org
	(use-package toc-org
		:ensure t
		:config
		(if (require 'toc-org nil t)
				(add-hook 'org-mode-hook 'toc-org-mode)
			;; enable in markdown, too
			(add-hook 'markdown-mode-hook 'toc-org-mode)
			(define-key markdown-mode-map (kbd "\C-c\C-o") 'toc-org-markdown-follow-thing-at-point)
			(warn "toc-org not found")))

	;; Org-Tempo - snippets for org mode blocks
	;; ref https://orgmode.org/manual/Structure-Templates.html
	(require 'org-tempo)

	;; #### Customize 'org-comment-string'
	;; This keyword indicates a subtree should not be included in org agenda
	;; An entry can be toggled between COMMENT and normal with
	;; ‘C-c ;’.
	(setq org-comment-string "BACKBURNER")

	;; ## Linux-Specific
	;; ========================================
	(when (eq system-type 'gnu/linux)
		(setq org-image-actual-width 400)
		)
	) ;; end ORG CORE

;; ## ORG PKMS FUNCTIONALITY
;; ========================================
(when (eq m-use-org-core t)
	(when (eq m-use-org-pkms t)
		(if (eq m-use-private-config t)
				(require 'm-private-pkms-config) ;; end private-config=t
			(setq org-directory (concat (getenv "HOME") "/Documents/")
						secondbrain-directory org-directory
						) ;; end private-config=nil
			) ;; end private-config IF statement

		(setq org-archive-location (concat secondbrain-archive ".orgarchive/%s_archive::"))

		;; ### ORG AGENDA
		;; ----------------------------------------
		;; Get agenda files recursively
		;; ref: https://stackoverflow.com/a/56172516
		(defun org-get-agenda-files-recursively (dir)
			"Get org agenda files from root DIR."
			(directory-files-recursively dir ".org$"))

		(defun org-set-agenda-files-recursively (dir)
			"Set org-agenda files from root DIR."
			(setq org-agenda-files 
						(org-get-agenda-files-recursively dir)))

		(defun org-add-agenda-files-recursively (dir)
			"Add org-agenda files from root DIR."
			(nconc org-agenda-files 
						 (org-get-agenda-files-recursively dir)))

		(setq org-agenda-files nil)
		(org-set-agenda-files-recursively org-directory)
		(org-add-agenda-files-recursively kb-directory)

		(setq org-agenda-span 'day)
		(setq org-agenda-todo-ignore-scheduled 'future)
		(setq org-agenda-custom-commands
					'(("n" "Agenda / NEXT / PROJ / WAITING / TODO"
						 ((agenda "" nil)
							(todo "NEXT" nil)
							(todo "PROJ" nil)
							(todo "WAITING" nil)
							(todo "TODO" nil)
							nil)
						 ((org-agenda-tag-filter-preset '("-IDEAS"))))))
		;; (if (eq m-is-for-work t)
		;; 		(add-to-list 'org-agenda-custom-commands
		;; 								 '(("w" "Work Agenda / NEXT / PROJ / WAITING / TODO"
		;; 										((agenda "" nil)
		;; 										 (todo "NEXT" nil)
		;; 										 (todo "PROJ" nil)
		;; 										 (todo "WAITING" nil)
		;; 										 (todo "TODO" nil)
		;; 										 nil)
		;; 										((org-agenda-tag-filter-preset '("WORK -IDEAS"))))
		;; 									 )))

		;; ### REFILE
		;; ----------------------------------------
		(setq org-refile-targets '((nil :maxlevel . 9)
															 (org-agenda-files :maxlevel . 9)
															 ))
		(setq org-refile-use-outline-path t)
		(setq org-outline-path-complete-in-steps nil)
		(setq org-refile-allow-creating-parent-nodes 'confirm)
		
		;; ### CAPTURE TEMPLATES
		;; ----------------------------------------
		(setq org-capture-templates
					'(
						("t" "TODO - Simple." entry
						 (file the-inbox)
						 "* TODO %?\n%u")
						("d" "TODO - With Deadline." entry
						 (file the-inbox)
						 "* TODO %?\n%u\nDEADLINE: %t")
						("n" "NEXT TASK." entry
						 (file the-inbox)
						 "* NEXT %?\n%u\nDEADLINE: %t")
						("p" "PROJECT." entry
						 (file the-planner)
						 "* PROJ %?")
						("j" "Journal Log." entry
						 (file+olp+datetree the-journal)
						 "* %?\n%U\n")
						("w" "Website" entry
						 (file the-inbox)
						 "* %a\n%U %?\n%:initial")
						)
					)

		;; ### ORG PROTOCOL
		;; ----------------------------------------
		(require 'org-protocol)
		(use-package org-protocol-capture-html
			:load-path "otherpackages/m-org-protocol-capture-html")

		;; ### ORG WEB TOOLS
		;; ----------------------------------------
		(use-package org-web-tools
			:ensure t
			)

		;; ### Tell Emacs where sqlite3.exe is stored (for Org Roam)
		;; ----------------------------------------
		(add-to-list 'exec-path "~/bin/sqlite")

		;; ### ORG ROAM
		;; ----------------------------------------
		;; Org Roam configuration based on Orgroam manual and
		;; https://github.com/nobiot/Zero-to-Emacs-and-Org-roam

		(use-package org-roam
			:ensure t
			:diminish org-roam-mode
			:config
			
			;; #### Define key bindings for Org-roam
			(global-set-key (kbd "<f12>") #'org-roam-buffer-toggle-display)
			(global-set-key (kbd "C-c n i") #'org-roam-insert)
			(global-set-key (kbd "C-c n /") #'org-roam-find-file)
			(global-set-key (kbd "C-c n b") #'org-roam-switch-to-buffer)
			(global-set-key (kbd "C-c n d") #'org-roam-find-directory)

			;; #### Roam directories and other settings
			(setq org-roam-directory secondbrain-directory
						org-roam-db-location "~/.emacs.d/machine-specific/org-roam.db"
						org-roam-buffer-no-delete-other-windows t ; make org-roam buffer sticky
						org-roam-file-exclude-regexp "*/xARCHIVES/*"
						)

			;; #### make roam use org ID links
			;; ref: https://org-roam.discourse.group/t/org-roam-major-redesign/1198/29
			(require 'org-id)
			(setq org-id-link-to-org-use-id t)
			(setq org-roam-prefer-id-links t)

			;; #### ORG-ROAM CAPTURE TEMPLATES
			(if (eq m-use-private-config t)
					(progn
						;; ##### LINUX CAPTURE TEMPLATES
						(when (eq system-type 'gnu/linux)
							(setq org-roam-capture-templates
										'(
											;; ###### NOTE
											("n" "Personal Note" plain (function org-roam--capture-get-point)
											 "%?"
											 :file-name "03-KB/KB_Personal/${slug}"
											 :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n* ${slug}"
											 :unnarrowed t)

											;; ###### CYBER NOTE
											("c" "Cyber Note" plain (function org-roam--capture-get-point)
											 "%?"
											 :file-name "03-KB/KB_Cyber/${slug}"
											 :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n* ${slug}"
											 :unnarrowed t)

											;; ###### PROJECT FILE
											("p" "Project" plain (function org-roam--capture-get-point)
											 "%?"
											 :file-name "01-pORG/PROJECT-${slug}"
											 :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n* ${slug}"
											 :unnarrowed t)

											;; ###### REF NOTE
											("r" "Personal Ref Note" plain (function org-roam--capture-get-point)
											 "%?"
											 :file-name "03-KB/KB_Personal/${slug}"
											 :head "#+title: ${title}\n#+ROAM_KEY: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n* ${slug}"
											 (string :format " %v" :value "${body}\n")
											 :unnarrowed t)

											;; ###### CYBER REF NOTE
											("e" "Cyber Ref Note" plain (function org-roam--capture-get-point)
											 "%?"
											 :file-name "03-KB/KB_Cyber/${slug}"
											 :head "#+title: ${title}\n#+ROAM_KEY: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n* ${slug}"
											 (string :format " %v" :value "${body}\n")
											 :unnarrowed t)
											
											))
							) ;; end Linux capture templates

						;; ##### MACOS CAPTURE TEMPLATES
						(when (equal system-type 'darwin)
							(setq org-roam-capture-templates
										'(
											;; ###### NOTE
											("n" "Work Note" plain (function org-roam--capture-get-point)
											 "%?"
											 :file-name "03-KB/KB_Work/${slug}"
											 :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:workkx.org][WorkKX]], \n\n* ${slug}"
											 :unnarrowed t)

											;; ###### CYBER NOTE
											("c" "Cyber Note" plain (function org-roam--capture-get-point)
											 "%?"
											 :file-name "03-KB/KB_Cyber/${slug}"
											 :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n* ${slug}"
											 :unnarrowed t)

											;; ###### PERSONAL NOTE
											("q" "Personal Note" plain (function org-roam--capture-get-point)
											 "%?"
											 :file-name "03-KB/KB_Personal/${slug}"
											 :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n* ${slug}"
											 :unnarrowed t)

											;; ###### PROJECT FILE
											("p" "Project" plain (function org-roam--capture-get-point)
											 "%?"
											 :file-name "01-ORG/wORG/PROJECT-${slug}"
											 :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:workkx.org][WorkKX]], \n\n* ${slug}"
											 :unnarrowed t)

											;; ###### REF NOTE
											("r" "Work Ref Note" plain (function org-roam--capture-get-point)
											 "%?"
											 :file-name "03-KB/KB_Work/${slug}"
											 :head "#+title: ${title}\n#+ROAM_KEY: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:workkx.org][WorkKX]], \n\n* ${slug}"
											 (string :format " %v" :value "${body}\n")
											 :unnarrowed t)

											;; ###### CYBER REF NOTE
											("e" "Cyber Ref Note" plain (function org-roam--capture-get-point)
											 "%?"
											 :file-name "03-KB/KB_Cyber/${slug}"
											 :head "#+title: ${title}\n#+ROAM_KEY: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n* ${slug}"
											 (string :format " %v" :value "${body}\n")
											 :unnarrowed t)
											))) ;; end Macos capture templates
						) ;; end 'then' statement (os-defined capture templates)
				;; ##### DEFAULT CAPTURE TEMPLATES (no OS specified)
				(setq org-roam-capture-templates
							'(
								;; ###### NOTE
								("n" "Note" plain (function org-roam--capture-get-point)
								 "%?"
								 :file-name "${slug}"
								 :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n* ${slug}"
								 :unnarrowed t)

								;; ###### CYBER NOTE
								("c" "Cyber Note" plain (function org-roam--capture-get-point)
								 "%?"
								 :file-name "${slug}"
								 :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n* ${slug}"
								 :unnarrowed t)

								;; ###### PROJECT FILE
								("p" "Project" plain (function org-roam--capture-get-point)
								 "%?"
								 :file-name "PROJECT-${slug}"
								 :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: \n\n* ${slug}"
								 :unnarrowed t)

								;; ###### REF NOTE
								("r" "Ref Note" plain (function org-roam--capture-get-point)
								 "%?"
								 :file-name "${slug}"
								 :head "#+title: ${title}\n#+ROAM_KEY: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n* ${slug}"
								 (string :format " %v" :value "${body}\n")
								 :unnarrowed t)

								;; ###### CYBER REF NOTE
								("e" "Cyber Ref Note" plain (function org-roam--capture-get-point)
								 "%?"
								 :file-name "${slug}"
								 :head "#+title: ${title}\n#+ROAM_KEY: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n* ${slug}"
								 (string :format " %v" :value "${body}\n")
								 :unnarrowed t)

								)) ;; end 'then' (i.e. default org-roam-capture-templates when OS not specified)
				) ;; end capture templates if statement
			;; #### ORG ROAM CAPTURE REF TEMPLATES
			(setq org-roam-capture-ref-templates
						'(
							;; ##### REF CAPTURE
							("R" "ref" plain (function org-roam-capture--get-point)
							 "%?"
							 :file-name "03-KB/KB_Personal/${slug}"
							 :head "#+title: ${title}\n#+roam_key: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n* ${slug}"
							 :unnarrowed t)

							;; ##### FULL PAGE CAPTURE
							("f" "full page capture" entry (function org-roam-capture--get-point)
							 "%(org-web-tools--url-as-readable-org \"${ref}\")"
							 :file-name "03-KB/KB_Personal/${slug}"
							 :head "#+title: ${title}\n#+roam_key: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n"
							 :unnarrowed t)

							;; ##### Immediate Fill Page Capture
							("i" "immediate full page capture" entry (function org-roam-capture--get-point)
							 "%(org-web-tools--url-as-readable-org \"${ref}\")"
							 :file-name "03-KB/KB_Personal/${slug}"
							 :head "#+title: ${title}\n#+roam_key: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n"
							 :immediate-finish t)

							;; ##### CYBER REF CAPTURE
							("cR" "Cyber ref" plain (function org-roam-capture--get-point)
							 "%?"
							 :file-name "03-KB/KB_Cyber/${slug}"
							 :head "#+title: ${title}\n#+roam_key: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n* ${slug}"
							 :unnarrowed t)

							;; ##### CYBER FULL PAGE CAPTURE
							("cf" "Cyber full page capture" entry (function org-roam-capture--get-point)
							 "%(org-web-tools--url-as-readable-org \"${ref}\")"
							 :file-name "03-KB/KB_Cyber/${slug}"
							 :head "#+title: ${title}\n#+roam_key: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n"
							 :unnarrowed t)

							;; ##### CYBER Immediate Full Page Capture
							("ci" "Cyber immediate full page capture" entry (function org-roam-capture--get-point)
							 "%(org-web-tools--url-as-readable-org \"${ref}\")"
							 :file-name "03-KB/KB_Cyber/${slug}"
							 :head "#+title: ${title}\n#+roam_key: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n"
							 :immediate-finish t)								 
							)
						) ;; end org-roam-capture-ref-templates
			) ;; end org-roam use-package

		;; #### Enable org-roam-mode
		(add-hook 'after-init-hook 'org-roam-mode)
		
		;; Since the org module lazy loads org-protocol (waits until an org URL is
		;; detected), we can safely chain `org-roam-protocol' to it.
		(use-package org-roam-protocol
			:ensure f
			:after org-protocol)
		
		;; #### Additional org-roam settings
		(setq org-roam-completion-system 'default
					org-roam-buffer-width 0.2
					org-roam-file-extensions (quote ("org"))
					)

;; NOT WORKING RIGHT!!!		;; #### ** Nroam
;; ;; - "nroam is a supplementary package for org-roam that replaces the backlink side buffer of Org-roam. Instead, it displays org-roam backlinks at the end of org-roam buffers. The user can also click a button to see unlinked occurrences of the buffer title (as defined by =org-roam-unlinked-references=)."
;; ;; - [[https://github.com/NicolasPetton/nroam.git][https://github.com/NicolasPetton/nroam.git]]
;; ;; # #+begin_src emacs-lisp
;;      (use-package nroam
;;        :load-path "otherpackages/nroam"
;;        :after org-roam
;;        :config
;;        (add-hook 'org-mode-hook #'nroam-setup-maybe)
;;      )
;; ;; # #+end_src

		;; ### PKMS SEARCH FUNCTIONS
		;; ----------------------------------------

		;; #### Deft (C-c n d)
		(use-package deft
			:ensure t
			:commands deft
			:bind
			("C-c n d" . deft)
			:init
			(setq deft-directory kb-directory
						deft-archive-directory secondbrain-archive
						deft-default-extension "org"
						;; de-couples filename and note title:
						deft-use-filename-as-title nil
						deft-use-filter-string-for-filename t
						;; disable auto-save
						deft-auto-save-interval 0
						;; converts the filter string into a readable file-name using kebab-case:
						deft-file-naming-rules
						'((noslash . "-")
							(nospace . "-")
							(case-fn . downcase))
						deft-recursive t
						deft-recursive-ignore-dir-regexp (concat "\\(?:\\.\\|\\.\\.\\)$"
																										 "\\|\\(?:"
																										 "xARCHIVES"
																										 "\\|Remediation"
																										 "\\|src"
																										 "\\)"
																										 )
						deft-strip-summary-regexp "\\`\\(.+\n\\)+\n" ;; https://github.com/jrblevin/deft/issues/75
						)
			(add-hook 'deft-mode-hook (lambda() (visual-line-mode -1)))
			(add-hook 'deft-mode-hook (lambda() (linum-mode -1)))
			:config
			(setq deft-separator " > "
						)
			)

		;; #### helm-org-rifle
		(use-package helm-org-rifle
			:ensure t
			:bind ("C-c f" . helm-org-rifle-agenda-files)
			)

		)) ;; end ORG PKMS

;; ## ORG EXPORT
;; ========================================

;; ### ox-gfm - Github Flavored Markdown exporter for Org Mode
;; ----------------------------------------
(use-package ox-gfm
	:ensure t
	:config
	(eval-after-load "org"
		'(require 'ox-gfm nil t))
	)
;; ### Disable Subscripts Export
;; ----------------------------------------
;; disable export of subscripts, meaning underscores like in 'my_file_name' won't result in subscripts
;; FYI - this can cause problems if doing lots of math export where subscripts are needed, though
;; ref: https://stackoverflow.com/questions/698562/disabling-underscore-to-subscript-in-emacs-org-mode-export?rq=1
(setq org-export-with-sub-superscripts nil)

;; ### Export .docx instead of .odt
;; ----------------------------------------
;; When I tell Org-Mode to export to ODT at my day job, I actually want DOCX.
;; source: https://www.reddit.com/r/orgmode/comments/6d396u/extending_orgmodes_orgexportasodt_function_to/
;; #### Macos
(when (eq system-type 'darwin)
	(setq org-odt-preferred-output-format "docx")
	)
(when (eq system-type 'windows-nt)
  (setq org-odt-preferred-output-format "docx")
  ;; "According to Chen Bin
	;; (http://blog.binchen.org/posts/how-to-take-screen-shot-for-business-people-efficiently-in-emacs.html),
  ;; the above should be sufficient on Linux, but he needed more setup on OSX.
	;; Let's see if I can adapt his code to Windows."
  (defun mlo/config-org-export-as-docx-via-odt ()
    (interactive)
    (let ((cmd "C:\\Program\ Files\\LibreOffice\\program\\soffice.exe"))
      (when (and (eq system-type 'windows-nt) (file-exists-p cmd))
				;; org v7
				(setq org-export-odt-convert-processes '
							(("LibreOffice" "C:\\Program\ Files\\LibreOffice\\program\\soffice.exe --headless --convert-to %f%x --outdir %d %i")))
				;; org v8/v9
				(setq org-odt-convert-processes '
							(("LibreOffice" "C:\\Program\ Files\\LibreOffice\\program\\soffice.exe --headless --convert-to %f%x --outdir %d %i"))))
      ))
  ;; Run the function I just defined to set up org-export-as-docx-via-odt.
  (mlo/config-org-export-as-docx-via-odt)
	)

;; ########################################
;; # DIRED
;; ########################################

;; ## Linux-Specific
;; ========================================
(when (eq system-type 'gnu/linux)
	(setq dired-listing-switches "-lahgG")
	)

;; ########################################
;; # PROGRAMMING LANGUAGES SUPPORT
;; ########################################

;; ## JSON
;; ========================================
(use-package json-mode
	:ensure t)
	

;; ########################################
;; # RANDOM / OTHER
;; ########################################

;; ## Configure Correct Browser on Windows
;; ========================================
(when (eq system-type 'windows-nt)
  (setq browse-url-browser-function 'browse-url-default-windows-browser))

;; ## Blogging with Ox-Hugo
;; ========================================
;; Ox-Hugo blogging package from https://github.com/kaushalmodi/ox-hugo
;; with additional setup from
;; https://www.shanesveller.com/blog/2018/02/13/blogging-with-org-mode-and-ox-hugo/

(use-package ox-hugo
  :ensure t
  :after ox)

;; ############################################################
;; # INIT.EL ENDS HERE
;; ############################################################

;; \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;; ////////////////////////////////////////////////////////////
;; \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;; ////////////////////////////////////////////////////////////
;; \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

;; ########################################
;; # ARCHIVE -- UNUSED FUNCTIONS
;; ########################################

;; # UNUSED MODULES
;; ----------------------------------------
;; (require 'm-ido)
;; (require 'm-recentf-ido) ;; find recent files. requires ido
;; (require 'm-helm)
;; (require 'm-org-journal)
;; (require 'm-org-ref)
;; (require 'm-pdf-tools)
;; (require 'm-org-noter)
;; (require 'm-org-cv)
;; (require 'm-enhanced-opendocument-text-exporter)
;; (require 'my-old-stuff)

;; ????????????????????????????????????????

;; LITERATE CONFIG
;; Starting transition to literate config
;; (org-babel-load-file "~/.emacs.d/config.org")

;; ????????????????????????????????????????

;; ### All The Icons
;; ----------------------------------------

;; #TODO -- Figure out where this is called -- I think it's actually needed for one of the sidebars I'm not using

;; https://github.com/domtronn/all-the-icons.el
;; NOTE: YOU MUST INSTALL THE RESOURCE FONTS FOR THIS TO WORK
;; TO DO THIS, call M-x all-the-icons-install-fonts
;; (use-package all-the-icons
;;   :ensure t)

;; ????????????????????????????????????????

;; #### Helpful
;; Associated with Ivy-Swiper-Counsel
;; (use-package helpful
;;   :custom
;;   (counsel-describe-function-function #'helpful-callable)
;;   (counsel-describe-variable-function #'helpful-variable)
;;   :bind
;;   ([remap describe-function] . counsel-describe-function)
;;   ([remap describe-command] . helpful-command)
;;   ([remap describe-variable] . counsel-describe-variable)
;;   ([remap describe-key] . helpful-key))

;; ????????????????????????????????????????
