(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ns-confirm-quit t)
 '(org-agenda-skip-unavailable-files t)
 '(org-blank-before-new-entry '((heading) (plain-list-item)))
 '(org-capture-bookmark nil)
 '(org-startup-folded 'content)
 '(org-web-tools-pandoc-sleep-time 2.0)
 '(package-selected-packages
	 '(json-mode graphviz-dot-mode exec-path-from-shell helm-org-rifle toml-mode toc-org ox-gfm pandoc-mode powershell org-noter pdf-view-restore pdf-tools which-key use-package tron-legacy-theme transpose-frame projectile ox-hugo org-web-tools org-roam org-plus-contrib olivetti markdown-mode magit ivy-rich helm-org helm-descbinds diminish deft dashboard counsel all-the-icons))
 '(tab-width 2))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background nil))))
 '(org-ellipsis ((t (:foreground "gray40" :underline nil)))))
