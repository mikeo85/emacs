(use-package ox-moderncv
    :load-path "~/.emacs.d/otherpackages/org-cv/"
    :init (require 'ox-moderncv))

(provide 'm-org-cv)
