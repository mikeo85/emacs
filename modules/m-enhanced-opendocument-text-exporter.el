(add-to-list 'package-archives
	     '("ox-odt" . "https://kjambunathan.github.io/elpa/"))

(use-package ox-odt
  :ensure t
  :config
  (custom-set-variables
   '(org-odt-convert-process "LibreOffice")
   '(org-odt-preferred-output-format "odt")
   '(org-odt-transform-processes
     '(("Optimize Column Width of all Tables"
	"soffice" "--norestore" "--invisible" "--headless"
	"macro:///OrgMode.Utilities.OptimizeColumnWidth(%I)")
       ("Update All"
	"soffice" "--norestore" "--invisible" "--headless"
	"macro:///OrgMode.Utilities.UpdateAll(%I)")
       ("Reload"
	"soffice" "--norestore" "--invisible" "--headless"
	"macro:///OrgMode.Utilities.Reload(%I)")
       )
     )
   )
  )
  
(provide 'm-enhanced-opendocument-text-exporter)
