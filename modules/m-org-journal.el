(use-package org-journal
  :ensure t
  :bind
  ("C-c n j" . org-journal-new-entry)
  :custom
  (org-journal-date-prefix "#+title: ")
  (org-journal-file-format "%Y-%m-%d-Journal.org")
  (org-journal-dir (concat org-directory "03-JOURNAL/"))
  (org-journal-date-format "%A, %d %B %Y"))

(provide 'm-org-journal)
