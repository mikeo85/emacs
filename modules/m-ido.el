;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;IDO MODE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package ido
  :ensure t
  :config
  (setq ido-everywhere t)
  (setq ido-enable-flex-matching t)
  (ido-mode 1)
  (setq ido-use-filename-at-point 'guess)
  (setq ido-file-extensions-order '(".org" ".txt" ".py" ".emacs" ".xml" ".el" ".ini" ".cfg" ".cnf"))
  (setq ido-ignore-extensions t)
  )

(provide 'm-ido)
