;; ORG-REF
(use-package org-ref
  :ensure t
    :config
    (setq
         org-ref-completion-library 'org-ref-ivy-cite
         org-ref-get-pdf-filename-function 'org-ref-get-pdf-filename-helm-bibtex
         org-ref-default-bibliography (list (concat org-directory "zotLib.bib"))
         org-ref-bibliography-notes (concat org-directory "04-NOTES/bibnotes.org")
         org-ref-note-title-format "* TODO %y - %t\n :PROPERTIES:\n  :Custom_ID: %k\n  :NOTER_DOCUMENT: %F\n :ROAM_KEY: cite:%k\n  :AUTHOR: %9a\n  :JOURNAL: %j\n  :YEAR: %y\n  :VOLUME: %v\n  :PAGES: %p\n  :DOI: %D\n  :URL: %U\n :END:\n\n"
         org-ref-notes-directory org-directory
         org-ref-notes-function 'orb-edit-notes
	 bibtex-completion-notes-path org-directory
	 bibtex-completion-bibliography (concat org-directory "zotLib.bib")
	 bibtex-completion-pdf-field "file"
	 bibtex-completion-notes-template-multiple-files
	 (concat
	  "#+TITLE: ${title}\n"
	  "#+ROAM_KEY: cite:${=key=}\n"
	  "* TODO Notes\n"
	  ":PROPERTIES:\n"
	  ":Custom_ID: ${=key=}\n"
	  ":NOTER_DOCUMENT: %(orb-process-file-field \"${=key=}\")\n"
	  ":AUTHOR: ${author-abbrev}\n"
	  ":JOURNAL: ${journaltitle}\n"
	  ":DATE: ${date}\n"
	  ":YEAR: ${year}\n"
	  ":DOI: ${doi}\n"
	  ":URL: ${url}\n"
	  ":END:\n\n"
	  )
	 )
    )

(provide 'm-org-ref)
;; END ORG-REF
