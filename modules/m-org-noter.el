(use-package org-noter
  :ensure t
  :after (:any org pdf-view)
  :config
  (setq org-noter-separate-notes-from-heading t
	org-noter-always-create-frame nil
   ;; The WM can handle splits
   org-noter-notes-window-location 'other-frame
   ;; Please stop opening frames
   org-noter-always-create-frame nil
   ;; I want to see the whole file
   org-noter-hide-other nil
   ;; Default path for org-noter notes should be my NOTES directory
   org-noter-notes-search-path (list org-roam-directory)
   )
  )

(provide 'm-org-noter)
