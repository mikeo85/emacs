;;; my-old-stuff.el -- all my old config, shifted over to here

;; Basic Customization

; (setq initial-frame-alist (quote ((fullscreen . maximized))))

(setq redisplay-dont-pause t) ;; source: https://masteringemacs.org/article/improving-performance-emacs-display-engine
(setq sort-fold-case t) ;;Ignore case differences when sorting -- see: https://masteringemacs.org/article/sorting-text-line-field-regexp-emacs
(require 're-builder) ;; https://masteringemacs.org/article/re-builder-interactive-regexp-builder
(setq reb-re-syntax 'string);; https://masteringemacs.org/article/re-builder-interactive-regexp-builder

(setq-default comment-start "# ") ;; https://stackoverflow.com/a/15124770/11064439

(global-set-key (kbd "<f12>") 'eval-buffer)

(global-unset-key (kbd "C-x C-z"))
(global-unset-key (kbd "C-h h"))

;; ;; Modus vivendi theme -- https://gitlab.com/protesilaos/modus-themes
;; (use-package modus-vivendi-theme
;;   :ensure t
;;   :config (load-theme `modus-vivendi t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BACKUP
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq backup-directory-alist
        '(("." . "~/.emacs.d/backup/")))
  (setq backup-by-copying t)
  (setq version-control t)
  (setq delete-old-versions t)
  (setq kept-new-versions 6)
  (setq kept-old-versions 2)
  (setq create-lockfiles nil)


  (defun prot/shell-multi ()
    "Spawn a new instance of `shell' and give it a unique name
based on the directory of the current buffer."
    (interactive)
    (let* ((parent (if (buffer-file-name)
                       (file-name-directory (buffer-file-name))
                     default-directory))
           (name (car (last (split-string parent "/" t)))))
      (with-current-buffer (shell)
        (rename-buffer
         (generate-new-buffer-name (concat "*shell: " name "*"))))))
  :bind (("<s-return>" . shell)
         ("<s-S-return>" . prot/shell-multi)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Flyspell (Spell check)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package flyspell
  :ensure t
  :commands (ispell-word
	     flyspell-buffer
	     flyspell-mode
	     flyspell-region)
  :config
  (setq flyspell-issue-message-flag nil)
  (setq flyspell-issue-welcome-flag nil)
  (setq ispell-program-name "aspell")
  (setq ispell-dictionary "en_US"))

;; Export Applications
;;(setq org-odt-styles-dir "/home/mike/.emacs.d/org/")
(when (is_linux)
  (setq org-file-apps
      '((auto-mode . emacs)
	("\\.mm\\'" . default)
	("\\.x?html?\\'" . "firefox %s")
	("\\.pdf\\'" . default))))


(defun markdown-convert-buffer-to-org ()
    "Convert the current buffer's content from markdown to orgmode format and save it with the current buffer's file name but with .org extension."
    (interactive)
    (shell-command-on-region (point-min) (point-max)
                             (format "pandoc -f markdown -t org -o %s %s"
                                     (concat (file-name-sans-extension (buffer-file-name)) ".org") (buffer-file-name))))



  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DOCKER
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (use-package docker
;;   :ensure t
;;   :bind ("C-c d" . docker))
;; (use-package docker-compose-mode
;;   :ensure t)



(provide 'my-old-stuff) ;; must be at end of file

;;; my-old-stuff.el ends here
