;; ;; Org Roam configuration based on Orgroam manual and
;; ;; https://github.com/nobiot/Zero-to-Emacs-and-Org-roam

;; ;;; Tell Emacs where sqlite3.exe is stored
;; (add-to-list 'exec-path "~/bin/sqlite")

;; (use-package org-roam
;;   :ensure t
;;   :diminish org-roam-mode
;;   :config
;;   ;;; Define key bindings for Org-roam
;;   (global-set-key (kbd "<f12>") #'org-roam-buffer-toggle-display)
;;   (global-set-key (kbd "C-c n i") #'org-roam-insert)
;;   (global-set-key (kbd "C-c n /") #'org-roam-find-file)
;;   (global-set-key (kbd "C-c n b") #'org-roam-switch-to-buffer)
;;   (global-set-key (kbd "C-c n d") #'org-roam-find-directory)
  
;;   (setq org-roam-directory secondbrain-directory
;;     	org-roam-db-location "~/.emacs.d/machine-specific/org-roam.db"
;;         org-roam-buffer-no-delete-other-windows t ; make org-roam buffer sticky
;; 	org-roam-file-exclude-regexp "*/xARCHIVES/*"
;; 	)

;; 	;; make roam use org ID links: https://org-roam.discourse.group/t/org-roam-major-redesign/1198/29
;; 	(require 'org-id)
;; 	(setq org-id-link-to-org-use-id t)
;; 	(setq org-roam-prefer-id-links t) 

  ;; ;; *DEFAULT* ORG ROAM CAPTURE TEMPLATES
  ;; ;; See the OS-specific config files for customized settings
	;; (when (null m-use-private-config)
  ;; (setq org-roam-capture-templates
	;; '(
	;;   ;; NOTE
	;;   ("n" "Note" plain (function org-roam--capture-get-point)
	;;    "%?"
	;;    :file-name "${slug}"
	;;    :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n* ${slug}"
	;;    :unnarrowed t)

	;;   ;;CYBER NOTE
	;;   ("c" "Cyber Note" plain (function org-roam--capture-get-point)
	;;    "%?"
	;;    :file-name "${slug}"
	;;    :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n* ${slug}"
	;;    :unnarrowed t)

	;;   ;; PROJECT FILE
	;;   ("p" "Project" plain (function org-roam--capture-get-point)
	;;    "%?"
	;;    :file-name "PROJECT-${slug}"
	;;    :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: \n\n* ${slug}"
	;;    :unnarrowed t)

  ;;         ;; REF NOTE
	;;   ("r" "Ref Note" plain (function org-roam--capture-get-point)
	;;    "%?"
	;;    :file-name "${slug}"
	;;    :head "#+title: ${title}\n#+ROAM_KEY: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n* ${slug}"
	;;    (string :format " %v" :value "${body}\n")
	;;    :unnarrowed t)

	;;   ;; CYBER REF NOTE
	;;   ("e" "Cyber Ref Note" plain (function org-roam--capture-get-point)
	;;    "%?"
	;;    :file-name "${slug}"
	;;    :head "#+title: ${title}\n#+ROAM_KEY: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n* ${slug}"
	;;    (string :format " %v" :value "${body}\n")
	;;    :unnarrowed t)

	;;   )) ;; end org-roam-capture-templates
	) ;; end when (null m-use-private-config)

    ;; ;; ORG ROAM CAPTURE REF TEMPLATES
    ;; (setq org-roam-capture-ref-templates
	  ;; '(
	  ;;   ;; REF CAPTURE
	  ;;   ("R" "ref" plain (function org-roam-capture--get-point)
	  ;;    "%?"
	  ;;    :file-name "03-KB/KB_Personal/${slug}"
	  ;;    :head "#+title: ${title}\n#+roam_key: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n* ${slug}"
	  ;;    :unnarrowed t)

	  ;;   ;; FULL PAGE CAPTURE
	  ;;   ("f" "full page capture" entry (function org-roam-capture--get-point)
	  ;;    "%(org-web-tools--url-as-readable-org \"${ref}\")"
	  ;;    :file-name "03-KB/KB_Personal/${slug}"
	  ;;    :head "#+title: ${title}\n#+roam_key: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n"
	  ;;    :unnarrowed t)

	  ;;   ;; Immediate Fill Page Capture
	  ;;   ("i" "immediate full page capture" entry (function org-roam-capture--get-point)
	  ;;    "%(org-web-tools--url-as-readable-org \"${ref}\")"
	  ;;    :file-name "03-KB/KB_Personal/${slug}"
	  ;;    :head "#+title: ${title}\n#+roam_key: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n"
	  ;;    :immediate-finish t)

	  ;;   ;; CYBER REF CAPTURE
	  ;;   ("cR" "Cyber ref" plain (function org-roam-capture--get-point)
	  ;;    "%?"
	  ;;    :file-name "03-KB/KB_Cyber/${slug}"
	  ;;    :head "#+title: ${title}\n#+roam_key: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n* ${slug}"
	  ;;    :unnarrowed t)

	  ;;   ;; CYBER FULL PAGE CAPTURE
	  ;;   ("cf" "Cyber full page capture" entry (function org-roam-capture--get-point)
	  ;;    "%(org-web-tools--url-as-readable-org \"${ref}\")"
	  ;;    :file-name "03-KB/KB_Cyber/${slug}"
	  ;;    :head "#+title: ${title}\n#+roam_key: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n"
	  ;;    :unnarrowed t)

	  ;;   ;; CYBER Immediate Full Page Capture
	  ;;   ("ci" "Cyber immediate full page capture" entry (function org-roam-capture--get-point)
	  ;;    "%(org-web-tools--url-as-readable-org \"${ref}\")"
	  ;;    :file-name "03-KB/KB_Cyber/${slug}"
	  ;;    :head "#+title: ${title}\n#+roam_key: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n"
	  ;;    :immediate-finish t)

	  ;;   )) ;; end org-roam-capture-ref-templates

  ) ;; end use-package

(add-hook 'after-init-hook 'org-roam-mode)

;; Since the org module lazy loads org-protocol (waits until an org URL is
;; detected), we can safely chain `org-roam-protocol' to it.
(use-package org-roam-protocol
  :ensure f
  :after org-protocol)

(setq org-roam-completion-system 'default
      org-roam-buffer-width 0.2
      org-roam-file-extensions (quote ("org"))
      )

(provide 'm-org-roam)
