;;; m-linux.el -- Linux-specific configuration
(when (eq system-type 'gnu/linux)

  ;; SYSTEM CONFIG STUFF
  ;; (setq org-image-actual-width 400)
  ;; ;; (setq dired-listing-switches "-lahgG")

	;; ;; ORG ROAM CAPTURE TEMPLATES
	;; (when (eq m-use-private-config t)
  ;;   (setq org-roam-capture-templates
	;;   '(
	;;     ;; NOTE
	;;     ("n" "Personal Note" plain (function org-roam--capture-get-point)
	;;      "%?"
	;;      :file-name "03-KB/KB_Personal/${slug}"
	;;      :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n* ${slug}"
	;;      :unnarrowed t)

	;;     ;;CYBER NOTE
	;;     ("c" "Cyber Note" plain (function org-roam--capture-get-point)
	;;      "%?"
	;;      :file-name "03-KB/KB_Cyber/${slug}"
	;;      :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n* ${slug}"
	;;      :unnarrowed t)

	;;     ;; PROJECT FILE
	;;     ("p" "Project" plain (function org-roam--capture-get-point)
	;;      "%?"
	;;      :file-name "01-pORG/PROJECT-${slug}"
	;;      :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n* ${slug}"
	;;      :unnarrowed t)

	;;     ;; REF NOTE
	;;     ("r" "Personal Ref Note" plain (function org-roam--capture-get-point)
	;;      "%?"
	;;      :file-name "03-KB/KB_Personal/${slug}"
	;;      :head "#+title: ${title}\n#+ROAM_KEY: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n* ${slug}"
	;;      (string :format " %v" :value "${body}\n")
	;;      :unnarrowed t)

	;;     ;; CYBER REF NOTE
	;;     ("e" "Cyber Ref Note" plain (function org-roam--capture-get-point)
	;;      "%?"
	;;      :file-name "03-KB/KB_Cyber/${slug}"
	;;      :head "#+title: ${title}\n#+ROAM_KEY: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n* ${slug}"
	;;      (string :format " %v" :value "${body}\n")
	;;      :unnarrowed t)
	    
	;;     )) ;; end org-roam-capture-templates

    ;; ORG ROAM CAPTURE REF TEMPLATES
		;; See modules/m-org-roam.el

    )) ;; end initial WHEN statement

;; ----------------------------------------
(provide 'm-linux)
;;; end m-linux.el
