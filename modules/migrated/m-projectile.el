(use-package projectile
  :diminish projectile-mode
  :ensure t
  )

(projectile-mode +1)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

(provide 'm-projectile)
