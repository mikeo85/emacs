;; Ox-Hugo blogging package from https://github.com/kaushalmodi/ox-hugo
;; with additional setup from
;; https://www.shanesveller.com/blog/2018/02/13/blogging-with-org-mode-and-ox-hugo/

(use-package ox-hugo
  :ensure t
  :after ox)

(provide 'm-ox-hugo)
