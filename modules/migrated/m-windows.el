;;; m-windows.el -- Windows-specific configuration
(when (eq system-type 'windows-nt)

  (setq browse-url-browser-function 'browse-url-default-windows-browser)

  ;;  ;; When I tell Org-Mode to export to ODT at my day job, I actually want DOCX.
  ;; ;; source: https://www.reddit.com/r/orgmode/comments/6d396u/extending_orgmodes_orgexportasodt_function_to/
  ;; (setq org-odt-preferred-output-format "docx")
  ;; ;; According to Chen Bin (http://blog.binchen.org/posts/how-to-take-screen-shot-for-business-people-efficiently-in-emacs.html),
  ;; ;; the above should be sufficient on Linux, but he needed more setup on OSX. Let's see if I can adapt his code to Windows.
  ;; (defun config-org-export-as-docx-via-odt ()
  ;;   (interactive)
  ;;   (let ((cmd "C:\\Program\ Files\\LibreOffice\\program\\soffice.exe"))
  ;;     (when (and (eq system-type 'windows-nt) (file-exists-p cmd))
  ;;     ;; org v7
	;; (setq org-export-odt-convert-processes '
	;;       (("LibreOffice" "C:\\Program\ Files\\LibreOffice\\program\\soffice.exe --headless --convert-to %f%x --outdir %d %i")))
  ;;     ;; org v8/v9
	;; (setq org-odt-convert-processes '
	;;       (("LibreOffice" "C:\\Program\ Files\\LibreOffice\\program\\soffice.exe --headless --convert-to %f%x --outdir %d %i"))))
  ;;     ))
  ;; ;; Run the function I just defined to set up org-export-as-docx-via-odt.
  ;; (config-org-export-as-docx-via-odt)
  
  )
;; ----------------------------------------
(provide 'm-windows)
;;; end m-windows.el
