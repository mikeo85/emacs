(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook)
  (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*"))
	;; Set the title
	dashboard-banner-logo-title "What Good Shall I Do This Day?"
	;; Set the banner
	dashboard-startup-banner "~/.emacs.d/modules/mountain.png"
	;; Value can be
	;; 'official which displays the official emacs logo
	;; 'logo which displays an alternative emacs logo
	;; 1, 2 or 3 which displays one of the text banners
	;; "path/to/your/image.png" which displays whatever image you would prefer

	;; Content is not centered by default. To center, set
	;; dashboard-center-content t

	;; To disable shortcut "jump" indicators for each section, set
	;; dashboard-show-shortcuts nil

	dashboard-items '(
			  (agenda . 10)
			  ;; (projects . 5)
			  ;; (bookmarks . 5)
			  (recents  . 10)
			  ;; (registers . 5)	
		  )
	dashboard-set-navigator t
	dashboard-set-init-info t
	;; dashboard-init-info "This is an init message!"

	;; A randomly selected footnote will be displayed. To disable it:
	;; dashboard-set-footer nil
	;; Or set custom dashboard footnotes:
	dashboard-footer-icon ""
	dashboard-footer-messages (quote (
"
     Do not conform yourselves to this age,
     but be transformed by the renewal of your mind,
     that you may discern what is the will of God,
     what is good and pleasing and perfect."
"
     For whoever wishes to save his life will lose it,
     but whoever loses his life for my sake will find it. 
     What profit would there be for one
     to gain the whole world and forfeit his life?"
"
     None of us lives for oneself, and no one dies for oneself.
     For if we live, we live for the Lord,
     and if we die, we die for the Lord;
     so then, whether we live or die, we are the Lord’s."
"
     And let thy feet millenniums hence
     be set in the midst of knowledge.
     (Tennyson)"
"
     Sometimes faith is understanding
     that you cannot understand."
"
     All great things are simple,
     and many can be expressed in a single word: 
     freedom; justice; honor; duty; mercy; hope.
     (Churchill)"
"
     We need to learn to set our course by the stars,
     not by every passing ship.
     (Bradley)"
"
     I will never leave you nor forsake you.
     Be strong and courageous, because you will lead…
     Have I not commanded you? Be strong and courageous.
     Do not be terrified; do not be discouraged, 
     for the Lord your God will be with you wherever you go."
"
     What saves a man is to take a step. Then another step.
     It is always the same step, but you have to take it.
     (Antoine de Saint-Exupery)"
"
     Happiness is different from pleasure.
     Happiness has something to do with 
     struggling and enduring and accomplishing.
     (George Sheehan)"
"
     The credit belongs to the man
     who is actually in the arena."
					  ))
	)
  )

(provide 'm-dashboard)
