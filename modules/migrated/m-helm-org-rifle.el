(use-package helm-org-rifle
	:ensure t
	:bind ("C-c f" . helm-org-rifle-agenda-files)
	)

(provide 'm-helm-org-rifle)
