(use-package deft
  :ensure t
  :commands deft
  :bind
  ("C-c n d" . deft)
  :init
  (setq deft-directory kb-directory
	deft-archive-directory secondbrain-archive
	deft-default-extension "org"
        ;; de-couples filename and note title:
        deft-use-filename-as-title nil
        deft-use-filter-string-for-filename t
        ;; disable auto-save
        deft-auto-save-interval 0
        ;; converts the filter string into a readable file-name using kebab-case:
        deft-file-naming-rules
        '((noslash . "-")
          (nospace . "-")
          (case-fn . downcase))
	deft-recursive t
	deft-recursive-ignore-dir-regexp (concat "\\(?:\\.\\|\\.\\.\\)$"
						 "\\|\\(?:"
						 "xARCHIVES"
						 "\\|Remediation"
						 "\\|src"
						 "\\)"
						 )
	deft-strip-summary-regexp "\\`\\(.+\n\\)+\n" ;; https://github.com/jrblevin/deft/issues/75
	)
  (add-hook 'deft-mode-hook (lambda() (visual-line-mode -1)))
  (add-hook 'deft-mode-hook (lambda() (linum-mode -1)))
  :config
  (setq deft-separator " > "
	)
  )

(provide 'm-deft)
