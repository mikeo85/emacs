;;; m-ui-theme-config.el

;; ;; THEME
;; ;; (Optional) Setting `custom-safe-themes' to t.
;; ;; This prevents Emacs from asking if it is safe to load the theme.
;; (setq custom-safe-themes t)

;; ;; ;; Subatomic Theme - https://github.com/cryon/subatomic-theme
;; ;; (use-package subatomic-theme
;; ;;   :ensure t
;; ;;   :config (load-theme 'subatomic t))
;; ;; (setq subatomic-high-contrast t)
;; ;; (setq subatomic-more-visible-comment-delimiters t)

;; ;; emacs-tron-theme
;; (add-to-list 'custom-theme-load-path "~/.emacs.d/otherpackages/emacs-tron-theme")
;; (load-theme 'tron t)
;; (if (equal system-type 'darwin)
;; 	(progn (set-face-attribute 'default nil :height 180))
;; 	(progn (set-face-attribute 'default nil :height 160))
;; )

;; ;; FONTS
;; ;; Primary Font Settings - Use Hack Font: https://sourcefoundry.org/hack/
;; ;; abcdefghijklmnopqrstuvwxyz 12345
;; ;; ABCDEFGHIJKLMNOPQRSTUVWXYZ 67890
;; ;; {}[]()<>$*-+=/#_%^@\&|~?'"`!,.;:
;; ;;(add-to-list 'default-frame-alist '(font . "Hack"))
;; ;;(set-face-attribute 'default nil
;; ;;		    :family "Hack"
;; ;;		    :font "Hack-16")
;; ;; (set-frame-font "20" nil t)
;; ;;(set-frame-font "16" nil t)
;; ;; (set-face-attribute 'default nil :height 160)
;; ;;(set-face-attribute 'default nil :font "Hack-16")
;; (when (member "Hack" (font-family-list))
;;   (add-to-list 'initial-frame-alist '(font . "Hack-18"))
;;   (add-to-list 'default-frame-alist '(font . "Hack-18"))
;; )

;; (setq inhibit-startup-message t) ;; hide the startup message
;; (tool-bar-mode -1)
;; (scroll-bar-mode -1)

;; (global-linum-mode t);; enable line numbers globally
;; (eval-after-load "linum"
;;   '(set-face-attribute 'linum nil :height 120)) ;; https://unix.stackexchange.com/questions/29786/font-size-issues-with-emacs-in-linum-mode/146781#146781

;; ;; Disable line numbers for some modes
;; (dolist (mode '(term-mode-hook
;; 		shell-mode-hook
;; 		eshell-mode-hook))
;;   (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; (blink-cursor-mode 0)
;; (setq ring-bell-function 'ignore)
;; (fset 'yes-or-no-p 'y-or-n-p) ;;https://masteringemacs.org/article/making-deleted-files-trash-can
;; (setq delete-by-moving-to-trash t) ;; source: https://masteringemacs.org/article/making-deleted-files-trash-can

;; (setq use-file-dialog nil)
;; (setq use-dialog-box t)  ;; only for mouse events

;; ;; KEYBINDINGS
;; (define-key global-map (kbd "RET") 'newline-and-indent)
;; (global-set-key (kbd "C-z") 'undo) ; Ctrl+z
;; (global-set-key (kbd "C-o") 'other-window) ;; Mastering Emacs book p. 92
;; ;; Windmove package (ref ME p. 92)
;; ;; Allows you to cycle windows in cardinal directions
;; ;; Switch windows with shift key by pressing S-<left>, S-<right>, S-<up>, S-<down>
;; (windmove-default-keybindings)

;; ;; UTF-8 please
;; (setq locale-coding-system 'utf-8) ; pretty
;; (set-terminal-coding-system 'utf-8) ; pretty
;; (set-keyboard-coding-system 'utf-8) ; pretty
;; (set-selection-coding-system 'utf-8) ; please
;; (setq-default buffer-file-coding-system 'utf-8-unix)
;; (setq-default default-buffer-file-coding-system 'utf-8-unix)
;; (set-default-coding-systems 'utf-8-unix)
;; (prefer-coding-system 'utf-8-unix)
;; (when (eq system-type 'windows-nt)
;;   (set-clipboard-coding-system 'utf-16le-dos))

;; (setq show-paren-delay 0) ;; Show Paren Mode - https://www.emacswiki.org/emacs/ShowParenMode
;; (show-paren-mode 1)

;; (use-package transpose-frame
;;   :ensure t
;;   :config
;;   (global-set-key (kbd "<f10>") 'transpose-frame))
  
  
;; ;; Stop Emacs from "littering" your folder with #file# and file~
;; ;; These configurations will stop Emacs creating backup files, etc.
;; ;; I find them annoying. Some of you may find them useful as backup
;; ;; files -- in this case, there is no problem keeping them.
;; ;; Avoid #file.org# to appear
;; (auto-save-visited-mode)
;; (setq create-lockfiles nil)
;; ;; Avoid filename.ext~ to appear
;; (setq make-backup-files nil)

;; ;; Make ESC quit prompts
;; (global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; ;;; rainbow delimeters show nesting of parentheses
;; (use-package rainbow-delimeters
;;   :ensure t
;;   :hook (prog-mode . rainbow-delimeters-mode))

;; which key -- guide to available key binding completions
;; (use-package which-key
;;   :ensure t
;;   :init (which-key-mode)
;;   :diminish which-key-mode
;;   :config
;;   (setq which-key-idle-delay 1))

;; ;; Remove-DOS-EOL: Do not show ^M in files containing mixed UNIX and DOS line endings.
;; ;; Ref: https://stackoverflow.com/questions/730751/hiding-m-in-emacs
;; (defun remove-dos-eol ()
;;   "Do not show ^M in files containing mixed UNIX and DOS line endings."
;;   (interactive)
;;   (setq buffer-display-table (make-display-table))
;;   (aset buffer-display-table ?\^M []))
;; (add-hook 'text-mode-hook 'remove-dos-eol)

;; ;; Prompt before killing Emacs
;; (setq confirm-kill-emacs 'y-or-n-p)

;; ----------------------------------------------
;; (provide 'm-ui-theme-config)
;;; m-ui-theme-config.el ends here
