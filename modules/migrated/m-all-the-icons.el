(use-package all-the-icons
  :ensure t)

;; NOTE: YOU MUST INSTALL THE RESOURCE FONTS FOR THIS TO WORK
;; TO DO THIS, call M-x all-the-icons-install-fonts

(provide 'm-all-the-icons)
