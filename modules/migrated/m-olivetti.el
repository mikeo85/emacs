;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; OLIVETTI - Writing Mode
;; Look & Feel for long-form writing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package olivetti
  :ensure t)

(global-set-key (kbd "C-c o") 'olivetti-mode)
(setq olivetti-body-width 90)

;; Enable Olivetti for text-related mode such as Org Mode
;; (add-hook 'text-mode-hook 'olivetti-mode)

(provide 'm-olivetti)
