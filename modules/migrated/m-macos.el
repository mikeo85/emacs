;; CUSTOMIZATIONS for MAC OS SYSTEMS

;; ;; https://medium.com/really-learn-programming/configuring-emacs-on-macos-a6c5a0a8b9fa
(when (equal system-type 'darwin)

  ;; ;; SYSTEM CONFIG STUFF
  ;; (setq mac-command-modifier 'meta)
  ;; (setq mac-option-modifier 'meta)
  ;; (setq mac-right-option-modifier 'control)
  ;; (global-set-key (kbd "<home>") 'move-beginning-of-line)
  ;; (global-set-key (kbd "<end>") 'end-of-visual-line)
  ;; ;; (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
  ;; (add-to-list 'default-frame-alist '(ns-appearance . dark))
  ;; (set-fontset-font t 'symbol (font-spec :family "Apple Symbols") nil 'prepend)
  ;; (set-fontset-font t 'symbol (font-spec :family "Apple Color Emoji") nil 'prepend)
  ;; (setq shell-file-name "/bin/zsh")
	;; ;; Sync Emacs PATH from SHELL. Ref: https://github.com/purcell/exec-path-from-shell
	;; (use-package exec-path-from-shell
	;; 	:ensure t
	;; 	:init
	;; 	(exec-path-from-shell-initialize))
  ;; (setq default-directory "~/")
  ;;   (when (featurep 'ns)
  ;; (defun ns-raise-emacs ()
  ;;   "Raise Emacs."
  ;;   (ns-do-applescript "tell application \"Emacs\" to activate"))(defun ns-raise-emacs-with-frame (frame)
  ;;   "Raise Emacs and select the provided frame."
  ;;   (with-selected-frame frame
  ;;     (when (display-graphic-p)
  ;;       (ns-raise-emacs))))(add-hook 'after-make-frame-functions 'ns-raise-emacs-with-frame)(when (display-graphic-p)
	;; 										      (ns-raise-emacs)))
  ;; (setq org-image-actual-width 600)

	;; ;; ## Manage mouse scroll speed
	;; ;; ----------------------------------------
	;; (setq mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control) . nil)))
	;; (setq mouse-wheel-progressive-speed nil)
	
	;; ORG ROAM CAPTURE TEMPLATES
	(when use-private-config
    (setq org-roam-capture-templates
	  '(
	    ;; NOTE
	    ("n" "Work Note" plain (function org-roam--capture-get-point)
	     "%?"
	     :file-name "03-KB/KB_Work/${slug}"
	     :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:workkx.org][WorkKX]], \n\n* ${slug}"
	     :unnarrowed t)

	    ;;CYBER NOTE
	    ("c" "Cyber Note" plain (function org-roam--capture-get-point)
	     "%?"
	     :file-name "03-KB/KB_Cyber/${slug}"
	     :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n* ${slug}"
	     :unnarrowed t)

	    ;; PERSONAL NOTE
	    ("q" "Personal Note" plain (function org-roam--capture-get-point)
	     "%?"
	     :file-name "03-KB/KB_Personal/${slug}"
	     :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:personalkx.org][PersonalKX]], \n\n* ${slug}"
	     :unnarrowed t)

	    ;; PROJECT FILE
	    ("p" "Project" plain (function org-roam--capture-get-point)
	     "%?"
	     :file-name "01-ORG/wORG/PROJECT-${slug}"
	     :head "#+TITLE: ${title}\n#+roam_alias: \n#+STARTUP: showall\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:workkx.org][WorkKX]], \n\n* ${slug}"
	     :unnarrowed t)

	    ;; REF NOTE
	    ("r" "Work Ref Note" plain (function org-roam--capture-get-point)
	     "%?"
	     :file-name "03-KB/KB_Work/${slug}"
	     :head "#+title: ${title}\n#+ROAM_KEY: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:workkx.org][WorkKX]], \n\n* ${slug}"
	     (string :format " %v" :value "${body}\n")
	     :unnarrowed t)

	    ;; CYBER REF NOTE
	    ("e" "Cyber Ref Note" plain (function org-roam--capture-get-point)
	     "%?"
	     :file-name "03-KB/KB_Cyber/${slug}"
	     :head "#+title: ${title}\n#+ROAM_KEY: ${ref}\n#+roam_alias: \n#+STARTUP: showall\n- source :: ${ref}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n- tags :: [[file:cyberkx.org][CyberKX]], \n\n* ${slug}"
	     (string :format " %v" :value "${body}\n")
	     :unnarrowed t)
	    
	    )) ;; end org-roam-capture-templates

    ;; ORG ROAM CAPTURE REF TEMPLATES
		;; See modules/m-org-roam.el

    )) ;; end initial WHEN statement

(provide 'm-macos)
