;; ;; ORG MODE
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;; (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
;; ;; (use-package org
;; ;;   :ensure org-plus-contrib)

;; ;; ;; The following lines are always needed.  Choose your own keys.
;; ;; (global-set-key "\C-cl" 'org-store-link)
;; ;; (global-set-key "\C-ca" 'org-agenda)
;; ;; (global-set-key "\C-cc" 'org-capture)
;; ;; (global-set-key "\C-cb" 'org-switchb)

;; ;; (add-hook 'org-mode-hook 'visual-line-mode)
;; ;; (add-hook 'org-mode-hook 'org-indent-mode)

;; ;; ;; ;; 'secondbrain-directory' and 'secondbrain-archive' are defined in m-private-config
;; ;; ;; (setq org-archive-location (concat secondbrain-archive ".orgarchive/%s_archive::")
;; ;; ;;       )

;; ;; (setq org-todo-keywords
;; ;;       '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
;; ;; 	(sequence "PROJ(p)" "GOAL(g)" "|" "COMPLETE(c)")
;; ;; 	(sequence "WAITING(w)" "BACKBURNER(b)" "IDEA(i)" "|" "CANCELLED(C)")))

;; ;; ;; Auto-update tags whenever the state is changed
;; ;; ;; ~Disabled~ because I don't use it
;; ;; (setq org-todo-state-tags-triggers
;; ;;       '(("IDEA" ("ideas" . t))))
;; ;; ;;       '(("CANCELLED" ("CANCELLED" . t))
;; ;; ;; 	("WAITING" ("BACKBURNER") ("WAITING" . t))
;; ;; ;; 	("BACKBURNER" ("WAITING") ("BACKBURNER" . t))
;; ;; ;; 	(done ("WAITING") ("BACKBURNER"))
;; ;; ;; 	("TODO" ("WAITING") ("CANCELLED") ("BACKBURNER"))
;; ;; ;; 	("NEXT" ("WAITING") ("CANCELLED") ("BACKBURNER"))
;; ;; ;; 	("PROJ" ("WAITING") ("CANCELLED") ("BACKBURNER"))
;; ;; ;; 	("AOR" ("WAITING") ("CANCELLED") ("BACKBURNER"))
;; ;; ;; 	("DONE" ("WAITING") ("CANCELLED") ("BACKBURNER"))))

;; ;; (setq org-agenda-skip-deadline-if-done t)
;; ;; (setq org-agenda-skip-scheduled-if-done t)
;; ;; (setq org-deadline-warning-days 0)
;; ;; (setq org-scheduled-warning-days 0)
;; ;; (setq org-fontify-done-headline t)
;; ;; (setq org-log-done (quote time))
;; ;; (setq org-log-done-with-time t)
;; ;; (setq org-priority-start-cycle-with-default nil)
;; ;; (setq org-support-shift-select nil)
;; ;; (setq org-id-method 'ts) ;; Use timestamps for org IDs: https://org-roam.discourse.group/t/org-roam-major-redesign/1198/28

;; ;; ;; Display properties
;; ;; (setq org-cycle-separator-lines 0)
;; ;; (setq org-tags-column 8)
;; ;; (setq org-latex-prefer-user-labels t)
;; ;; (setq org-ellipsis " [+]")
;; ;; (custom-set-faces '(org-ellipsis ((t (:foreground "gray40" :underline nil)))))
;; ;; (setq org-display-inline-images t)
;; ;; (setq org-redisplay-inline-images t)
;; ;; (setq org-startup-with-inline-images "inlineimages")

;; ;; ;; Dim blocked tasks (and other settings)
;; ;; (setq org-enforce-todo-dependencies t)

;; ;; Get agenda files recursively -- ref: https://stackoverflow.com/a/56172516
;; (defun org-get-agenda-files-recursively (dir)
;;   "Get org agenda files from root DIR."
;;   (directory-files-recursively dir ".org$"))

;; (defun org-set-agenda-files-recursively (dir)
;;   "Set org-agenda files from root DIR."
;;   (setq org-agenda-files 
;;     (org-get-agenda-files-recursively dir)))

;; (defun org-add-agenda-files-recursively (dir)
;;   "Add org-agenda files from root DIR."
;;   (nconc org-agenda-files 
;;     (org-get-agenda-files-recursively dir)))

;; (setq org-agenda-files nil)
;; (org-set-agenda-files-recursively org-directory)
;; (org-add-agenda-files-recursively kb-directory)

;; ;; (setq org-agenda-files
;; ;;       (delete-dups (setq org-agenda-files
;; ;; 			 (append (list
;; ;; 				  secondbrain-directory
;; ;; 				  org-directory
;; ;; 				  (concat secondbrain-directory notes-dir)
;; ;; 				  (concat secondbrain-directory cyber-notes-dir)
;; ;; 				  (concat secondbrain-directory alt-agenda-dir)
;; ;; 				  (concat secondbrain-directory alt-notes-dir)
;; ;; 				  )
;; ;; 				 org-agenda-files)
;; ;; 			 )
;; ;; 		   )
;; ;;       )

;; (setq org-agenda-span 'day)
;; (setq org-agenda-todo-ignore-scheduled 'future)
;; (setq org-agenda-custom-commands
;;       '(("n" "Agenda / NEXT / PROJ / WAITING / TODO"
;; 				 ((agenda "" nil)
;; 					(todo "NEXT" nil)
;; 					(todo "PROJ" nil)
;; 					(todo "WAITING" nil)
;; 					(todo "TODO" nil)
;; 					nil)
;; 				 ((org-agenda-tag-filter-preset '("-IDEAS"))))
;; 				;; ("w" "Work Agenda / NEXT / PROJ / WAITING / TODO"
;; 				;;  ((agenda "" nil)
;; 				;; 	(todo "NEXT" nil)
;; 				;; 	(todo "PROJ" nil)
;; 				;; 	(todo "WAITING" nil)
;; 				;; 	(todo "TODO" nil)
;; 				;; 	nil)
;; 				;;  ((org-agenda-tag-filter-preset '("WORK -IDEAS"))))
;; 				))

;; ;; REFILE
;; (setq org-refile-targets '((nil :maxlevel . 9)
;; 			   (org-agenda-files :maxlevel . 9)
;; 			   ))
;; (setq org-refile-use-outline-path t)
;; (setq org-outline-path-complete-in-steps nil)
;; (setq org-refile-allow-creating-parent-nodes 'confirm)

;; ;; CAPTURE TEMPLATES
;; (setq org-capture-templates
;;       '(
;; 	("t" "TODO - Simple." entry
;;          (file the-inbox)
;;          "* TODO %?\n%u")
;; 	("d" "TODO - With Deadline." entry
;;          (file the-inbox)
;;          "* TODO %?\n%u\nDEADLINE: %t")
;; 	("n" "NEXT TASK." entry
;;          (file the-inbox)
;;          "* NEXT %?\n%u\nDEADLINE: %t")
;; 	("p" "PROJECT." entry
;; 	 (file the-planner)
;; 	 "* PROJ %?")
;; 	("j" "Journal Log." entry
;; 	 (file+olp+datetree the-journal)
;; 	 "* %?\n%U\n")
;; 	("w" "Website" entry
;;          (file the-inbox)
;; 	 "* %a\n%U %?\n%:initial")
;; 	)
;;       )

;; (require 'org-protocol)
;; (use-package org-protocol-capture-html
;;  :load-path "otherpackages/m-org-protocol-capture-html")

;; ;; Function to archive all DONE tasks in a subtree
;; ;; ref :https://stackoverflow.com/questions/6997387/how-to-archive-all-the-done-tasks-using-a-single-command
;; (defun org-archive-done-tasks ()
;;   (interactive)
;;   (org-map-entries
;;    (lambda ()
;;      (org-archive-subtree)
;;      (setq org-map-continue-from (org-element-property :begin (org-element-at-point))))
;;    "/DONE" 'tree))

;; ;; ox-gfm - Github Flavored Markdown exporter for Org Mode
;; (use-package ox-gfm
;;   :ensure t
;;   :config
;;   (eval-after-load "org"
;;     '(require 'ox-gfm nil t))
;;   )

;; ;; toc-org -- ORG TABLE OF CONTENTS GENERATOR
;; ;;;; To use this, add a :TOC: tag to a headline.
;; ;;;; Every time you save, the first headline with the :TOC: tag will
;; ;;;; be updated with the current table of contents.
;; ;;;; See additional instructions at https://github.com/snosov1/toc-org
;; (use-package toc-org
;;   :ensure t
;;   :config
;;   (if (require 'toc-org nil t)
;;     (add-hook 'org-mode-hook 'toc-org-mode)
;;     ;; enable in markdown, too
;;     (add-hook 'markdown-mode-hook 'toc-org-mode)
;;     (define-key markdown-mode-map (kbd "\C-c\C-o") 'toc-org-markdown-follow-thing-at-point)
;;   (warn "toc-org not found")))

;; ;; Org-Tempo - snippets for org mode blocks
;; ;; ref https://orgmode.org/manual/Structure-Templates.html
;; (require 'org-tempo)

;; ;; disable export of subscripts, meaning underscores like in 'my_file_name' won't result in subscripts
;; ;; FYI - this can cause problems if doing lots of math export where subscripts are needed, though
;; ;; ref: https://stackoverflow.com/questions/698562/disabling-underscore-to-subscript-in-emacs-org-mode-export?rq=1
;; (setq org-export-with-sub-superscripts nil)

;; ;; Customize 'org-comment-string'
;; ;; This keyword indicates a subtree should not be included in org agenda
;; ;; An entry can be toggled between COMMENT and normal with
;; ;; ‘C-c ;’.
;; (setq org-comment-string "BACKBURNER")
			
(provide 'm-org-core)
