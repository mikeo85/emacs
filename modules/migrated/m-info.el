;;; m-info.el - general personal info
(setq user-full-name "Mike Owens"
      user-mail-address "mikeowens@fastmail.com")

(provide 'm-info)
;;; m-info.el ends here
