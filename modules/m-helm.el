;; heavily taken from:
;; -https://emacs-helm.github.io/helm
;; -https://github.com/thierryvolpiatto/emacs-tv-config/blob/master/init-helm.el
;; -https://github.com/bbatsov/prelude/blob/master/modules/prelude-helm.el
;; -https://tuhdo.github.io/helm-intro.html

(use-package helm
  :diminish
  :ensure t
  :config
  (require 'helm-config)
  (setq helm-input-idle-delay                     0.01
        helm-reuse-last-window-split-state        t
        helm-always-two-windows                   t
        helm-split-window-inside-p                nil
        helm-commands-using-frame                 '(completion-at-point
                                                    helm-apropos
                                                    helm-eshell-prompts helm-imenu
                                                    helm-imenu-in-all-buffers)
        helm-actions-inherit-frame-settings       t
        helm-use-frame-when-more-than-two-windows nil
        helm-use-frame-when-dedicated-window      nil
        helm-frame-background-color               "DarkSlateGray"
        helm-show-action-window-other-window      'left
        helm-allow-mouse                          t
        helm-move-to-line-cycle-in-source         t
        helm-autoresize-max-height                80 ; it is %.
        helm-autoresize-min-height                20 ; it is %.
        helm-follow-mode-persistent               t
        helm-candidate-number-limit               500)
  )

(use-package helm-mode
  :init
  (add-hook 'helm-mode-hook
            (lambda ()
              (setq completion-styles
                    (cond ((assq 'helm-flex completion-styles-alist)
                           '(helm-flex)) ;; emacs-26.
                          ((assq 'flex completion-styles-alist)
                           '(flex)))))) ;; emacs-27+.
  :diminish
  :config
  (helm-mode 1)
  (setq helm-completing-read-handlers-alist
        '((xref-find-references . helm-completing-read-default-find-tag)
          (write-file . helm-read-file-name-handler-1)
          (basic-save-buffer . helm-read-file-name-handler-1)
          (find-tag . helm-completing-read-default-find-tag)
          (xref-find-definitions . helm-completing-read-default-find-tag)
          (xref-find-references . helm-completing-read-default-find-tag)
          (tmm-menubar)
          (mu4e-view-save-attachment-multi . helm-read-file-name-handler-1)
          (mu4e-view-save-attachment-single . helm-read-file-name-handler-1)
          (cancel-debug-on-entry)
          (org-capture . helm-org-completing-read-tags)
          (org-set-tags . helm-org-completing-read-tags)
          (dired-do-rename . helm-read-file-name-handler-1)
          (dired-do-copy . helm-read-file-name-handler-1)
          (dired-do-symlink . helm-read-file-name-handler-1)
          (dired-do-relsymlink . helm-read-file-name-handler-1)
          (dired-do-hardlink . helm-read-file-name-handler-1)
          (basic-save-buffer . helm-read-file-name-handler-1)
          (write-file . helm-read-file-name-handler-1)
          (write-region . helm-read-file-name-handler-1))))

(use-package helm-adaptive
  :config
  (setq helm-adaptive-history-file nil)
  (helm-adaptive-mode 1))

(use-package helm-utils
  :config
  ;; Popup buffer-name or filename in grep/moccur/imenu-all etc...
  ;(helm-popup-tip-mode 1)
  (setq helm-highlight-matches-around-point-max-lines   '(30 . 30)
        helm-window-show-buffers-function #'helm-window-mosaic-fn)
  (add-hook 'find-file-hook 'helm-save-current-pos-to-mark-ring))

(use-package helm-sys
  :commands (helm-top)
  :config (helm-top-poll-mode 1))

(use-package helm-info
  :bind ("C-h r" . helm-info-emacs))

(use-package helm-buffers
  :config
  (setq helm-buffers-favorite-modes
        (append helm-buffers-favorite-modes '(picture-mode artist-mode))
        helm-buffers-fuzzy-matching       t
        helm-buffer-skip-remote-checking  t
        helm-buffer-max-length            22
        helm-buffers-end-truncated-string "…"
        helm-buffers-maybe-switch-to-tab  t
        helm-mini-default-sources '(helm-source-buffers-list
                                    helm-source-buffer-not-found))

  (define-key helm-buffer-map (kbd "C-d") 'helm-buffer-run-kill-persistent)
  
  (cl-defmethod helm-setup-user-source ((source helm-source-buffers))
  "Adds additional actions to `helm-source-buffers-list'.
- Magit status."
  (setf (slot-value source 'candidate-number-limit) 300)
  (helm-aif (slot-value source 'action)
      (setf (slot-value source 'action)
        (helm-append-at-nth
         (if (symbolp it)
             (symbol-value it)
           it)
         '(("Diff buffers" . helm-buffers-diff-buffers)) 4)))
  (helm-source-add-action-to-source-if
   "Magit status"
   (lambda (candidate)
     (funcall helm-ls-git-status-command
              (with-current-buffer candidate default-directory)))
   source
   (lambda (candidate)
     (locate-dominating-file (with-current-buffer candidate default-directory)
                             ".git"))
   1)))

(use-package helm-descbinds
  :ensure t
  :config
  ;; C-h b, C-x C-h etc...
  (helm-descbinds-mode 1))

(use-package helm-lib
  :config
  (setq helm-scroll-amount 4)
  (helm-help-define-key "C-x" 'exchange-point-and-mark)
  (helm-help-define-key "C-l" 'recenter-top-bottom))


(use-package helm-grep
  :diminish
  :config
  (setq helm-pdfgrep-default-read-command
        "evince --page-label=%p '%f'"
        helm-grep-default-command
        "ack-grep -Hn --color --smart-case --no-group %e %p %f"
        helm-grep-default-recurse-command
        "ack-grep -H --color --smart-case --no-group %e %p %f"
        helm-grep-ag-command
        "rg --color=always --colors 'match:bg:yellow' --colors 'match:fg:black' --smart-case --no-heading --line-number %s %s %s"
        helm-grep-ag-pipe-cmd-switches
        '("--colors 'match:bg:yellow' --colors 'match:fg:black'")
        helm-grep-git-grep-command
        "git --no-pager grep -n%cH --color=always --exclude-standard --no-index --full-name -e %p -- %f")
  (add-hook 'helm-grep-mode-hook 'hl-line-mode)
  (define-key helm-grep-map   (kbd "C-M-a") 'helm/occur-which-func))

(use-package helm-occur
  :diminish
  :config
  (add-hook 'helm-occur-mode-hook 'hl-line-mode)
  (define-key helm-occur-map (kbd "C-M-a") 'helm/occur-which-func))

(use-package helm-elisp
  :diminish
  :config
  (setq helm-show-completion-display-function #'helm-display-buffer-in-own-frame
        helm-apropos-fuzzy-match              t
        helm-lisp-fuzzy-completion            t)
  (helm-multi-key-defun helm-multi-lisp-complete-at-point
      "Multi key function for completion in emacs lisp buffers.
First call indent, second complete symbol, third complete fname."
    '(helm-lisp-indent
      helm-lisp-completion-at-point
      helm-complete-file-name-at-point)
    0.3)
  (define-key emacs-lisp-mode-map (kbd "TAB") 'helm-multi-lisp-complete-at-point)
  (define-key lisp-interaction-mode-map (kbd "TAB") 'helm-multi-lisp-complete-at-point))

(use-package helm-locate
  :diminish
  :config
  (setq helm-locate-fuzzy-match t))

(use-package helm-org
  :ensure t
  :config
  (setq helm-org-headings-fontify t))

(use-package helm-elisp-package
  :diminish
  :config
  (setq helm-el-package-autoremove-on-start t
        helm-el-package-upgrade-on-start t))

(use-package helm-misc
  :diminish
  :config
  ;; Minibuffer history (Rebind to M-s).
  (customize-set-variable 'helm-minibuffer-history-key [remap next-matching-history-element])
  (helm-epa-mode 1))

;;; Global-map
;;
;;
(global-set-key (kbd "M-x")                          'undefined)
(global-set-key (kbd "M-x")                          'helm-M-x)
(global-set-key (kbd "M-y")                          'helm-show-kill-ring)
(global-set-key (kbd "C-x C-f")                      'helm-find-files)
(global-set-key (kbd "C-x C-r")                      'helm-recentf)
(global-set-key (kbd "C-h d")                        'helm-info-at-point)
(global-set-key (kbd "C-h i")                        'helm-info)
(global-set-key (kbd "C-h C-f")                      'helm-apropos)
(global-set-key (kbd "C-h a")                        'helm-apropos)
(define-key global-map [remap jump-to-register]      'helm-register)
(define-key global-map [remap list-buffers]          'helm-mini)
(define-key global-map [remap dabbrev-expand]        'helm-dabbrev)
(define-key global-map [remap find-tag]              'helm-etags-select)
(define-key global-map [remap xref-find-definitions] 'helm-etags-select)
(define-key global-map (kbd "M-g a")                 'helm-do-grep-ag)
(define-key global-map (kbd "M-g g")                 'helm-grep-do-git-grep)
(define-key global-map (kbd "M-g i")                 'helm-gid)

;; Avoid hitting forbidden directories when using find.
(add-to-list 'completion-ignored-extensions ".gvfs/")
(add-to-list 'completion-ignored-extensions ".dbus/")
(add-to-list 'completion-ignored-extensions "dconf/")
  

(provide 'm-helm)
